Network Scanner README.md

Written by: Adi Samooha and Noam Artsi


About the ptoject:
The project is a device that connect to the wifi network and scans the wifi packets and analizes them in order to check if attack is made.
All the result are stored and can be process into a log wich the user can see, save, and send them in email.

Attacks we supply a scan:
1.  MITM 
2.  Syn-Flood (only in unencrypted networks)
3.  DNS-Spoofing (only in unencrypted networks)
4.  DHCP-Spoofing (only in unencrypted networks)
5.  Smurf Attack (only in unencrypted networks)
6.  Evil twin 



Our project contains:
1. server that connect to the DB (runs on windows)
2. device program (runs on linux ubuntu)
3. auth program to run on a computer in the network  


requirements to run the server:
*  C#
*  Access DB

requirements to run the device:
*  python3
*  linux OS
*  Node JS
*  networkcard that supports monitor mode


steps in order to run the program:
1. clone our git repository and extract our device folder into the linux mechine 
2. extract the server folder into a windows mechine and creat a C# project using the additional files
3. link the DB into the server:
    <Br>
    a) open MTServer.cs
    <Br>
    b) change the path in line 40 into the path of the DB
4. add the linux mechine that will run the device as one of our product:
    <Br>
    a) open our DB
    <Br>
    b) go to ProductsTbl
    <Br>
    c) add the linux machine mac address
5. installing the needed packeges:
*  in order to install the packeges for the web site:
        a)open the web site folder in the device folder
        b)run the following command:`npm install --production`
*   make sure you got all the following libraries downloaded to you linux machine and linked to python3:
        <Br>
        1) scapy
        <Br>
        2) netifaces
        <Br>
        3) base64
        <Br>
        4) gi
        <Br>
        5) sys
        <Br>
        6) socket
        <Br>
        7) subprocess
        <Br>
        8) hashlib
        <Br>
        9) datetime
        <Br>
        10) csv
        <Br>
        11) threading 
        <Br>
        12) time
        <Br>
        13) os
        <Br>
        14) re
        <Br>
        15) smtplib
        <Br>
        16) email
        <Br>
        17) multiprocessing
        <Br>
        18) flask
        <Br>
        19) requests
        <Br>
        20) json
        <Br>
        21) collections
        <Br>
        22) urllib
        <Br>
        23) termcolor
        <Br>
        24) __future__
        <Br>
        25) statistics
*    in case you dont have all of the packeges:
        pip install the missing packeges

6. turn on the monitor iface on the linux machine
7. set the monitor iface and the managed iface in the configuration file:
    <Br>
    a) open device folder
    <Br>
    b) go to configuration.txt
    <Br>
    c) replase the "mon0" in line: "monitor:*:mon0" to your monitor iface name
    <Br>
    d) replase the "wlx503eaa7d0fd7" in line: "managed:*:wlx503eaa7d0fd7" to your managed iface nam
8. run the server on the windows machine
9. set server ip in configuration fil
10. connect the linux machine to interne
11. run the main.py file in device folder 
12. open web browser link is <deviceIP>:5000
13. reconnect the network you want to check though the browser (if the network have password some features might not work)
14. go to the following link <deviceIP>:3000

<B> start protect you wifi network



