//libreries we use in the project
const express = require('express');
const bodyParser = require('body-parser');
const net = require('net');
const os = require('os');
const sha256 = require('js-sha256');
const path = require('path');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const expressValidator = require('express-validator');
const flash = require('connect-flash');
const session = require('express-session');
const request = require('request');
const http = require('http');
const dateFormat = require('dateformat');
const url = require('url');
const Base64 = require('js-base64').Base64;
const pug = require('pug');
const fs = require('fs');

//creat new app
const app = express();
//secret key for tokens
const KEY_FOR_JWT = 'SoPeRSecrETKey';

//link design libreries to app
app.use(express.static(path.join(__dirname,'public')));

//link librery of images to the app
app.use('/images', express.static(__dirname + '/Images'));

//link the pug views to app
app.set('views',path.join(__dirname,'views'));

//set app engin to pug instead of html
app.set('view engine', 'pug')

//using bodyParser.json in order to read json data from requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//link cookies to app
app.use(cookieParser());
app.use(session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true,
}));

//link flashes (alerts) to the app
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

//linking express validator (in order to check inputs)
app.use(expressValidator({
  errorFormatter: function(param,msg,value){
    var namespace = param.split(".")
    , root =namespace.shift()
    , formParam = root;
    while(namespace.length){
      formParam += '[' + namespace.shift() + ']';
    }
    return{
      param : formParam,
      msg : msg,
      value : value
    };
  }
}));

//running server
asyncCallServer();



/**********************************************************************************/
//async function that runs the server
async function asyncCallServer() {
  await runServer();
}

function runServer() {
  return new Promise(resolve => {
      //handle get home page
      app.get("/",(req,res) => {
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){
          //cookie is incorrect or expired
          res.clearCookie('token');
        }

        let lblHello = ""; //variable that holds the user name

        //case the user dc set the variable to guest
        if(req.cookies.token == undefined){
          lblHello = "guest";
        }
        else{
          //read the username from the cookie
          lblHello = jwt.verify(req.cookies.token,KEY_FOR_JWT).username;
        }
        //rendering home page
        res.render('HomePage',{
          title:"Home Page",
          logedIn: req.cookies.token == undefined,
          username: lblHello
        });
      });

      app.get("/Information",(req,res) =>{
        ans = verifyToken(req);
        if(!ans){
          res.clearCookie('token');
        }
        let lblHello = "";
        if(req.cookies.token == undefined){
          lblHello = "guest";
        }
        else{
          lblHello = jwt.verify(req.cookies.token,KEY_FOR_JWT).username;
        }
        res.render('explanationPage',{
          title:"Explanation Page",
          logedIn: req.cookies.token == undefined,
          username: lblHello
        });
      })

      app.get("/ActiveScan",(req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){
          //cookie is incorrect or expired
          res.clearCookie('token');
          //set an alert
          req.flash('danger','You need to Login in order to activat scan');
          //redirecting home page
          return res.redirect('/');
        }
        //object that store the flags
        flags = {
          man_in_the_middle_flag : false,
          smurf_flag : false,
          dhcp_spoofing_flag : false,
          dns_spoofing_flag : false,
          ssl_strip_flag : false,
          evil_twin_flag : false,
          syn_flood_flag : false
        }
        setFlags(flags); //setting the flags
        console.log("flags:");
        console.log(flags);
        //extracting username out oof the cookie into a variable
        let username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username;
        //rendering ActiveScan page
        res.render('ActiveScan',{
          title:"Scans Control",
          logedIn: req.cookies.token == undefined,
          username: username,
          flags:flags
        });
      })

      //handle get Login Page
      app.get("/LoginPage",(req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){
          //cookie is incorrect or expired
          res.clearCookie('token');
        }
        else{ //case the cookie is correct redirect user to the home page
          //set an alert
          req.flash('danger','You need to Logout in order to get the Login page');
          //redirect user to the home page
          return res.redirect('/');
        }
        res.render('Login',{
          title:"Login",
          logedIn: req.cookies.token == undefined,
          username: 'guest'
        });
      });

      //handle post Login Page
      app.post("/LoginPage",async function(req,res){
        //checking the values of the post request
        req.checkBody('username',"Username is required [min 3 letters]").isLength({min:3,max:99});
        req.checkBody('username',"Username need to be only letters and numbers").isAlphanumeric();
        req.checkBody('password',"Password is required [min 6 chars]").isLength({min:6,max:99}).escape()

        //check if there are errors
        let errors = req.validationErrors();
        if(errors){
          //rerendering the login page with the errors
          res.render('Login',{
            title : "Login",
            errors : errors,
            logedIn: req.cookies.token == undefined,
            username:'guest'
          })
        }
        else{ //case there are no errors
          let username = req.body.username; //extract the username out of the request
          let password = req.body.password; //extract the password out of the request
          //let auth = req.body.authString; // TODO: UNCOMMENT ME !!!! //extract the authentication string out of the request
          let auth = "1" // TODO: DELETE ME !!!!
          console.log("got data")
          console.log(`before send: ${auth}`);
          //chacking the authentication string
          ans2 = connectMainScript("/auth/"+auth);
          console.log("after send");
          ans2.then((val)=>{
            console.log("Got ans from auth")
            console.log(val)
            console.log("in ans2 Promise")
            //check if the authentication string was good
            if(val.res === "True"){ //case its good
              let msgCode = "01"; //login msg type from the protocol
              let msg = msgCode + getMac() + padNum(username.length) + username + sha256(password); //build a login request to server
              //send the login request to server
              ans = getAnsFromServer(msg);
              ans.then((value)=>{
                console.log(`value: ${value}`);
                console.log(typeof value);
                //case there is error in the server
                if(value === "99D"){
                  //clearing Cookie
                  res.clearCookie('token');
                  //set an alert
                  req.flash('danger','Somthing went wrong...');
                  //redirecting home page
                  return res.redirect('/');
                }
                //case OK from server to the login req
                if(value === "02C0"){
                  let authPromise = checkUserAuth(username); //check if user has auth to the device
                  authPromise.then((val) =>{
                    if(val){ //case the user have auth to the device
                      setUsernameInConf(username) //write the username in conf file in order to use it in scans later
                    }
                    //creating Cookie to user that stors the username
                    jwt.sign({username},KEY_FOR_JWT,(err,token) =>{
                      console.log(`token: ${token}`);
                      //give cookie 24 hours to live
                      res.cookie('token',token,{maxAge:24*60*60*1000,httpOnly:true});
                      //set an alert
                      req.flash('success','Login successfully');
                      //redirect home page
                      return res.redirect("/");
                    })
                  })
                }
                else if(value === "02D1"){ //case user is allready connected to diffrent device
                  //set an alert
                  req.flash('danger','User already connected');
                  //redirect home
                  return res.redirect("/");
                }
                else if(value == "02D2"){ //case there ae wrong details
                  //set an alert
                  req.flash('danger','Wrong details');
                  //redirect login page
                  return res.redirect("/LoginPage");
                }
              })
            }
            else{ //case the authentication string was bad
              //set an alert
              req.flash('danger','Authentication failed...');
              //redirecting login page
              return res.redirect("/LoginPage");
            }
          });
        }
      });

      //handle get register page
      app.get("/Register",(req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){
          //cookie is incorrect or expired
          res.clearCookie('token');
        }
        else{ //case cookie is good
          //set an alert
          req.flash('danger','You need to Logout in order to register');
          //redirect home page
          return res.redirect('/');
        }
        //rendering register page
        res.render('Register',{
          title:"Register",
          logedIn: req.cookies.token == undefined,
          username: 'guest'
        });
      });

      //handle post register
      app.post("/Register",(req,res) =>{
        //checking the values of the post request
        req.checkBody('username',"Username is required [min 3 letters]").isLength({min:3,max:99});
        req.checkBody('username',"Username need to be only letters and numbers").isAlphanumeric();
        req.checkBody('password',"password is required [min 6 chars]").isLength({min:6,max:99}).escape();
        req.checkBody('email',"Email is required [min 10 chars]").isLength({min:10,max:99});
        req.checkBody('email',"Email needs to be mail format XXXX@XXXX.XXX").isEmail();
        req.checkBody('password', "Passwords are not the same").equals(req.body.repeatPassword);

        //check if there are errors
        let errors = req.validationErrors();
        if(errors){
          //rerendering the register page with the errors
          res.render('Register',{
            title : "Register",
            errors : errors,
            logedIn: req.cookies.token == undefined,
            username:'guest'
          })
        }
        else{
          let username = req.body.username; //extracting username out of request
          let password = req.body.password; //extracting password out of request
          let email = req.body.email; //extracting email out of request
          let day = req.body.day; //extracting day out of request
          let month = req.body.month; //extracting month out of request
          let year = req.body.year; //extracting year out of request
          let country = req.body.country; //extracting country out of request
          let msgCode = "03"; //register msg code in protocol
          //build a register request
          let msg = msgCode + getMac() + padNum(username.length) + username + sha256(password) + padNum(email.length) + email + padNum(parseInt(day)) + "/" + padNum(parseInt(month)) + "/" + year + padNum(country.length) + country;
          console.log("msg: ", msg);
          //send server the register msg
          ans = getAnsFromServer(msg);
          ans.then((value)=>{
            console.log(`value: ${value}`);
            console.log(typeof value);
            //case there is error in the server
            if(value === "99D"){
              //clearing cookie
              res.clearCookie('token');
              //set an alert
              req.flash('danger','Somthing went wrong...');
              //redirect home page
              return res.redirect('/');
            }
            if(value === "04C0"){ //case OK from server
                //set an alert
                req.flash('success','User added successfully');
                //redirecting LoginPage
                return res.redirect("/LoginPage");
              }
            else if(value === "04D1"){ //case of wrong inputs
              //set an alert
              req.flash('danger','Wrong Inputs');
              //redirect register page
              return res.redirect("/Register");
            }
            else if(value == "04D2"){ //case user exists
              //set an alert
              req.flash('danger','Username Exist');
              //redirect register page
              return res.redirect("/Register");
            }
          })
        }
      });

      //handle get Scans
      app.get("/Scans",(req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){ //cookie is incorrect or expired
          //clearing cookie
          res.clearCookie('token');
          //set an alert
          req.flash('danger','You need to Login in order to get the Scans page');
          //redirect home page
          return res.redirect('/');
        }
        else{
          //extracting username out of cookie
          let username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username;
          //get the mac of the device
          let mac = getMac();
          let msg = "07" + mac + padNum(username.length) + username; //creating get scans msg
          ans = getAnsFromServer(msg); // send the get scans msg to server
          ans.then((value)=>{
            //case there is error in the server
            if(value === "99D"){
              //clearing cookie
              res.clearCookie('token');
              //set an alert
              req.flash('danger','Somthing went wrong...');
              //redirect home page
              return res.redirect('/');
            }
            console.log("msg: " + value);
            ans = value.substring(0,3); //get the statuse of the server response
            value = value.replace(value.substring(0,3),""); //cutting the statuse of the response
            if(ans === "08D"){ //case user doesnt have auth to the device
              //set an alert
              req.flash('danger','You have no authorization to see the logs history');
              //redirect home page
              return res.redirect('/');
            }
            console.log("STRING:");
            console.log(value);
            //pass server response to function that will parse the response into files object
            files = handleFilesString(value);
            lblHello = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting the username out of cookie
            //rendering scans options
            res.render('scansOptions',{
              title:"Scans History",
              logedIn: req.cookies.token == undefined,
              username: lblHello,
              scans:files
            });
          })
        }
      });

      //handle get Authorization Change
      app.get("/AuthorizationChange",(req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){ //cookie is incorrect or expired
          //clearing the cookie
          res.clearCookie('token');
          //set an alert
          req.flash('danger','You need to Login in order to change authorizations');
          //redirect home page
          return res.redirect('/');
        }
        else{ //cookie is ok
            //rendering Authorization Change
            res.render('AuthorizationChange',{
              title:"Authorization Change",
              logedIn: req.cookies.token == undefined,
              username: jwt.verify(req.cookies.token,KEY_FOR_JWT).username
            });
          }
      });

      //handle post Authorization Change
      app.post("/AuthorizationChange",(req,res) =>{
        //checking the values of the post request
        req.checkBody('username',"Username is required [min 3 letters]").isLength({min:3,max:99});
        req.checkBody('username',"Username need to be only letters and numbers").isAlphanumeric();

        let username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting the username out of the cookie
        //check if there are errors
        let errors = req.validationErrors();
        if(errors){
          //rendering AuthorizationChange with errors
          return res.render('AuthorizationChange',{
            title : "AuthorizationChange",
            errors : errors,
            logedIn: req.cookies.token == undefined,
            username: username
          });
        }
        else{ //no errors
          let username2 = req.body.username; //extracting the wanted username out of the request
          let level = req.body.level; //extracting the auth level out of request
          msg = "11" + getMac() + padNum(username.length) + username + level + padNum(username2.length) + username2; //creat auth change request
          //sending the server the change auth request
          ans = getAnsFromServer(msg);
          ans.then((value)=>{
            //case there is error in the server
            if(value === "99D"){
              //clearing cookie
              res.clearCookie('token');
              //set an alert
              req.flash('danger','Somthing went wrong...');
              //redirecting home page
              return res.redirect('/');
            }
            if(value === "12C0"){ //case OK from server
              //set an alert
              req.flash('success','Authorization changed');
              //redirect home page
              return res.redirect('/');
            }
            else if(value === "12D1"){ //current user dont have admin auth to the device
              //set an alert
              req.flash('danger','You need to be an admin in order to authorized other people');
              //redirect home page
              return res.redirect('/');
            }
            else if (value === "12D2") { //case searched user wasnt found
                //set an alert
                req.flash('danger','user wasn\'t found');
                //rendering Authorization Change
                return res.render('AuthorizationChange',{
                  title : "AuthorizationChange",
                  logedIn: req.cookies.token == undefined,
                  username: username
                });
            }
            else if(value === "12D3"){ //case the will be no admins after the change
              //set an alert
              req.flash('danger','Unable to delete the only admin');
              //rendering Authorization Change
              return res.render('AuthorizationChange',{
                title : "AuthorizationChange",
                logedIn: req.cookies.token == undefined,
                username: username
              });
            }
            else{ //server returned unexpected response
              //set an alert
              req.flash('danger','Somthing went wrong...');
              //redirecting home page
              return res.redirect('/');
            }
          });
          }
        })

      //handle get specific scan
      app.get("/Scan/:id",(req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){ //cookie is incorrect or expired
          //clearing cookie
          res.clearCookie('token');
          //set an alert
          req.flash('danger','You need to Login in order to view scan details');
          //redirecting home page
          return res.redirect('/');
        }
        else{ //cookie is correct
          let username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting username out of the cookie
          var id = req.params.id; //extracting the id of the scan from the query string (url)
          console.log(id);
          let msg = "09" + getMac() + padNum(username.length) + username + id; //creat get scan request
          console.log("msg: " + msg);
          ans = getAnsFromServer(msg); //send server get scan request
          ans.then((value)=>{
            //case there is error in the server
            if(value === "99D"){
              //clearing cookie
              res.clearCookie('token');
              //set an alert
              req.flash('danger','Somthing went wrong...');
              //redirect home page
              return res.redirect('/');
            }
            ans = value.substring(0,3); //get the server response status
            value = value.replace(value.substring(0,3),""); //cutting the respose status
            if(ans === "10D"){ //case user doesnt have auth to view scan
              //set an alert
              req.flash('danger','You not authorized to view scan details');
              //redirect home page
              return res.redirect('/');
            }
            scan = handleFileString(value,id); //pass server response to function that will process it into scan object
            console.log("scan:")
            console.log(scan)
            lblHello = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting the username out of the cookie
            //rendering scanView
            res.render('scanView',{
              title:"Product Scan",
              logedIn: req.cookies.token == undefined,
              username: lblHello,
              scan:scan
            });
          })
        }
      });

      //handle get send scan mail
      app.get('/SendScanMail/:id',(req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){ //cookie is incorrect or expired
          //clearing cookie
          res.clearCookie('token');
          //set an alert
          req.flash('danger','You need to Login in order to send scan in mail');
          //redirect home page
          return res.redirect('/');
        }
        let username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting username out of cookie
        let responsePromise = checkUserAuth(username); //check user auth to the device
        console.log("after check...");
        responsePromise.then((val) =>{
          if(val){//case user have auth to device
            var id = req.params.id; //extract id out of query string (url)
            let msg = "09" + getMac() + padNum(username.length) + username + id; //creat get file request
            console.log("msg: " + msg);
            ans = getAnsFromServer(msg); //send get file request to server
            ans.then((value)=>{
              if(value === "99D"){ //case there is error in the server
                //clearing cookie
                res.clearCookie('token');
                //set an alert
                req.flash('danger','Somthing went wrong...');
                //redirect home page
                return res.redirect('/');
              }
              //get the response status
              ans = value.substring(0,3);
              //cutting the respose status
              value = value.replace(value.substring(0,3),"");
              if(ans === "10D"){ //case user dont have auth to the device
                //set an alert
                req.flash('danger','You not authorized to scan in mail');
                //redirecting home page
                return res.redirect('/');
              }
              scan = handleFileString(value,id); //pass server response to function that will process it into scan object
              const template = path.join(app.get('views'), 'emailView.pug'); //get the template of the email file
              const compiledFunction = pug.compileFile(template); //compile email file
              //converting the html render code to base 64
              htmlString = Base64.encode(compiledFunction({
                title:"Product Scan",
                scan:scan
              }))
              //connecting main script in order to send the mail
              ans = connectMainScript(url.format({
                 pathname:"/sendLogEmail",
                 query: {
                    "html": htmlString,
                    "user": username,
                    "mac" : getMac()
                  }
                })
              )
              ans.then((val)=>{
                if(val.res === "200"){ //case email sent successfully
                  //set an alert
                  req.flash('success','Email sent');
                  //redirecting home page
                  return res.redirect('/');
                }
                else{ //case there some kind of error
                  //set an alert
                  req.flash('danger','Somthing went wrong... \nmail did not send. try again');
                  //redirecting home page
                  return res.redirect('/');
                }
              })
            })
          }
          else{ //case user doesnt have auth to devise
            //set an alert
            req.flash('danger','You need to be authorized in order to send scan in mail');
            //redirecting home page
            return res.redirect('/');
          }
        })
      })

      //handle post change scan
      app.post("/changeScan",(req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){ //cookie is incorrect or expired
          //clearing cookie
          res.clearCookie('token');
          //set an alert
          req.flash('danger','You need to Login in order to view scan details');
          //redirect home page
          return res.redirect('/');
        }
        let username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting username out of cookie
        let responsePromise = checkUserAuth(username); //check user auth to the device
        console.log("after check...");
        responsePromise.then((val) =>{
          if(val){//case user have auth to device
            console.log(req.body);
            console.log(typeof(req.body.box1))
            console.log(req.body.box1)
            for(var key in req.body) { //itarete the body fileds
              if(req.body.hasOwnProperty(key)){
                if(key.includes("_toggle")){ //case there is a toggle inside currunt filed
                  scan = key.replace("_toggle",""); //cutt the toggle in order to remain with the scan name
                  console.log("got scan: " + scan);
                  ans = connectMainScript("/changeScan/"+scan); //connect main script to change the scan
                  ans.then((val)=>{
                    console.log(val);
                    let status = val.res; //get main script response
                    if(status == "200"){ //case OK
                      //set an alert
                      req.flash('success','Scan changed successfully!');
                      //redirecting ActiveScan
                      return res.redirect('/ActiveScan');
                    }
                    else{ // main process return error
                      //set an alert
                      req.flash('danger','Somthing wrong happend...');
                      //redirect Home Page
                      return res.redirect('/');
                    }
                  })
                }
              }
            }
          }
          else{ //case user doesnt have auth to device
            //set an alert
            req.flash('danger','You need to be authorized in order to start a scan');
            //redirecting home page
            return res.redirect('/');
          }
        })
      })

      // TODO: delete this:
      app.get("/MITM/1",(req,res) =>{
        ans = verifyToken(req);
        if(!ans){
          res.clearCookie('token');
          req.flash('danger','You need to Login in order to enter scan');
          return res.redirect('/');
        }
        let username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username;
        let responsePromise = checkUserAuth(username);
        console.log("after check...");
        responsePromise.then((val) =>{
          console.log("inside then");
          console.log(`val:${val}`);
          if(val){
            console.log("val == true");
            ans = connectMainScript("/MITM/1");
            ans.then((val)=>{
              console.log(val);
              res.redirect(url.format({
                 pathname:"/setScan",
                 query: {
                   attacks: JSON.stringify([
                              {
                                "MITM": {status:val.res.MITM.status,
                                  attacker: val.res.MITM.attacker,
                                  day: val.res.MITM.day,
                                  hour: val.res.MITM.hour
                                }
                              }
                            ])
                       }
              }));
            })
          }
          else{
            req.flash('danger','You need to be authorized in order to start a scan');
            return res.redirect('/');
          }
        })
      });

      //handle get setScan
      app.get("/setScan",(req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){ //cookie is incorrect or expired
          //clearing cookie
          res.clearCookie('token');
          //set an alert
          req.flash('danger','You need to Login in order to save scan');
          //redirect jome page
          return res.redirect('/');
        }
        console.log("req.query")
        console.log(req.query)
        lblHello = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting username out of cookie
        var results = []; //creating a var that hold dictionaries of attacks
        var routerMac = "";
        let jsonData = JSON.parse(req.query.attacks) //read the attacks out of query string (url)
        console.log("prased json:::")
        console.log(jsonData)
        //itarete the json data
        for(let index in jsonData){
          for(const key in jsonData[index]){
            if(jsonData[index].hasOwnProperty(key)){
              //pushing new dictionary of attack
              results.push({
                [key]:{
                  attacker:jsonData[index][key].attacker,
                  date:jsonData[index][key].day,
                  hour:jsonData[index][key].hour
                }
              })
            }
          }
        }
        console.log("results:::")
        console.log(results)
        //connect main script in order to get the router mac
        prmMainScript = connectMainScript("/routerMac");
        prmMainScript.then((val) => {
          console.log(val.res);
          console.log(typeof(val.res));
          routerMac = val.res; //extracting router mac out of main process response
          //rendering configScan
          return res.render('configScan',{
            title:"Config Scan",
            logedIn: req.cookies.token == undefined,
            username: lblHello,
            productMac : getMac(),
            date : dateFormat("dd/mm/yyyy"),
            routerMac : routerMac,
            results : results
          });
        })
        //case Promise failed
        .catch((rej)=>{
          //set an alert
          req.flash('danger','Somthing went wrong...');
          //redirecting home page
          return res.redirect('/');
        });
      })

      //handle post setScan
      app.post("/setScan",(req,res) =>{
        let results = [];
        let sta = ""
        let scanName,productMac,date,routerMac,attackName,attackStatus;
        let cnt = "";
        //verifing cookie
        flag = verifyToken(req);
        if(!flag){ //cookie is incorrect or expired
          //clearing cookie
          res.clearCookie('token');
          //set an alert
          req.flash('danger','You need to Login in order save scan');
          //redirecting home page
          return res.redirect('/');
        }
        let username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting username out of cookie
        let responsePromise = checkUserAuth(username); //check if user have authorization to the device
        console.log("after check...");
        responsePromise.then((val) =>{
          console.log("inside then");
          console.log(`val:${val}`);
          if(!val){ //case user isnt authorized to the device
            //set an alert
            req.flash('danger','You need to be authorized in order save scan');
            //redirecting home page
            return res.redirect('/');
          }
          //check scan name
          req.checkBody('scanName',"Scan name is required [only leters and numbers]").isLength({min:1,max:99}).matches(/^[a-z0-9 ]+$/i);
          let errors = req.validationErrors();
          if(errors){ //case scan name isnt allowed set the name to defautlt
            scanName = "Scan";
          }
          else{
            scanName = req.body.scanName; //extract scan name out of body
          }
          filePath = path.join(".", 'temp_log.csv'); //creating path object
          fs.writeFile(filePath, '', ()=>{})
          productMac = getMac(); //get device mac
          date = dateFormat("dd/mm/yyyy"); //get today date in specific format
          prmMainScript = connectMainScript("/routerMac"); //connect main script in order to get router mac
          prmMainScript.then((val) => {
            routerMac = val.res; //read router mac from main script response
            var data = req.body
            //itarete body
            for(var key in req.body) {
              if(req.body.hasOwnProperty(key)){
                if(key.includes("_name")){ //got a attack since the format is: `${resultName}_${cnt}_name`
                  holeName = key.replace("_name","").split("_")[0]; //extracting the name of the attack
                  cnt = key.replace("_name","").split("_")[1] //get the number of the attack cuz there might be 10 MITM there is cnt
                  console.log("check::")
                  //push the list dictionary of attack
                  results.push({
                    holeName:holeName,
                    attacker:req.body[holeName + "_" + cnt + "_attacker"],
                    date:req.body[holeName + "_" + cnt + "_date"],
                    hour:req.body[holeName + "_" + cnt + "_hour"]
                  })
                }
              }
            }
            console.log("post setScan -> results:")
            console.log(results)
            //process the data into scan and convert it to base 64
            logString = Base64.encode(processScanString(username,scanName,productMac,date,routerMac,results));
            console.log(`LONG STRING: ${logString}`)
            //connect the main script in order it to send the log
            ans = connectMainScript("/sendLog/"+logString);
            ans.then((value) => {
              console.log("value");
              console.log(value)
              console.log("value type:")
              console.log(typeof(value))
              //extract the response of the main script
              answer = value.res;
              console.log("answer:")
              console.log(answer)
              console.log("answer type:")
              console.log(typeof(answer))
                if(answer === "99D"){ //case server returned main script that there was an error
                  //clearing cookie
                  res.clearCookie('token');
                  //set an alert
                  req.flash('danger','Somthing went wrong...');
                  //redirect to home page
                  return res.redirect('/');
                }
                type = answer.substring(0,3); //taking response status
                answer = answer.replace(answer.substring(0,3),""); //cutting response status
                if(type === "06C"){ //scan has been added
                  key = answer.substring(0,32); //taking scan key
                  return res.redirect('/Scan/' + key); //redirect to scan page
                }
                else{ //case other error happend
                  //set an alert
                  req.flash('danger','Somthing went wrong...');
                  //redirect home page
                  return res.redirect('/');
                }
            })
          })
        })
      })

      app.get("/loading",(req,res)=>{
        res.render('loadingPage',{})
      })

      //handle get getNetworkScan
      app.get("/getNetworkScan", (req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){ //cookie is incorrect or expired
          //clearing cookie
          res.clearCookie('token');
          //set an alert
          req.flash('danger','You need to Login in order to view the current network status');
          //redirect to home page
          return res.redirect('/');
        }
        username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting username out of cookie
        ans = connectMainScript("/viewNetwork/"+username); //connect main script to view the network
        console.log("after send");
        ans.then((val)=>{
          console.log("in ans Promise");
          console.log("res:");
          console.log(val);
          console.log(JSON.parse(val.res));
          //rendering view network
          res.render('viewNetwork',{
            title:"Current network",
            logedIn: req.cookies.token == undefined,
            username: username,
            users : JSON.parse(val.res)
          })
        });
      })

      //handle get checkPassword
      app.get("/checkPassword",(req,res) =>{
        ans = verifyToken(req); //verifing cookie
        if(!ans){ //cookie is incorrect or expired
          //clearing cookie
          res.clearCookie('token');
          //set an alert
          req.flash('danger',"You need to Login in order to check the strength of the network\'s password");
          //redirect home page
          return res.redirect('/');
        }
        let username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting username out of cookie
        let responsePromise = checkUserAuth(username); //check user auth to the device
        console.log("after check...");
        responsePromise.then((val) =>{
          if(val){ //user have auth to device
            ans = connectMainScript("/passwordGrade"); //connect main script in order to check the strength of password
            ans.then((value) =>{
              console.log(value)
              //rendering passwordGrade
              res.render("passwordGrade",{
                title:"Password Grade",
                logedIn: req.cookies.token == undefined,
                username: username,
                totalGrade: value.res.password_ok,
                lengthGrade: value.res.length,
                digitGrade: value.res.digit,
                uppercaseGrade: value.res.uppercase,
                lowercaseGrade : value.res.lowercase,
                symbolGrade : value.res.symbol
              })
            })
          }
          else{
            //set an alert
            req.flash('danger',"You need to be authorized in order to check the strength of the network\'s password");
            //redirect home page
            return res.redirect('/');
          }
        })
      })

      //handle get saved log
      app.get("/getSavedLog",(req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){ //cookie is incorrect or expired
          //clearing cookie
          res.clearCookie('token');
          //set an alert
          req.flash('danger','You need to Login in order to save scan');
          //redirecting home page
          return res.redirect('/');
        }
        username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting username out of cookie
        let responsePromise = checkUserAuth(username); //check user auth to device
        console.log("after check...");
        responsePromise.then((val) =>{
          if(val){ //user have auth
            ans = connectMainScript("/filterLog"); //connect main script in order to filter log
            ans.then((value) =>{
              if(value.res == "True"){ //check if main process returned ok
                filePath = path.join(".", 'temp_log.csv'); //creating path object
                data = fs.readFileSync(filePath, {encoding: 'utf-8'}) //reading file sync way
                console.log(data);
                attacks = processSavedLog(data); //pass to function that process the data into scans object
                //redirect setScan with attacks in query
                res.redirect(url.format({
                  pathname:"/setScan",
                  query: {
                    attacks:  JSON.stringify(attacks)
                  }
                }));
              }
              else{ //no attack in log
                //notify the user
                req.flash('success','There are no events in the log. Your network doesn\'t seems to be under attack');
                //redirecting home page
                return res.redirect('/');
              }
            })
          }
          else{ //user doesnt have auth to device
            //set an alert
            req.flash('danger','You need to be authorized in order to get the log');
            //redirecting home page
            return res.redirect('/');
          }
        })
      })

      //handle get setConfiguration
      app.get("/setConfiguration",(req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){ //cookie is incorrect or expired
          //clearing cookie
          res.clearCookie('token');
          //set an alert
          req.flash('danger','You need to Login in order to configur the system');
          //redirecting home page
          return res.redirect('/');
        }
        let username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting username out of cookie
        let responsePromise = checkUserAuth(username); //check user auth to device
        console.log("after check...");
        responsePromise.then((val) =>{
        console.log(val)
        if(val){ //user have auth
          //rendering setConf
          res.render("setConf",{
            title:"Configure System",
            logedIn: req.cookies.token == undefined,
            username: username,
            serverIP: getServerIP(),
            managedIface: getManaged(),
            monIface: getMonIface(),
            routerMac: getRoterMac(),
            essid : getEssid()
          })
        }
        else{ //user doesnt have auth to device
          //set an alert
          req.flash('danger','You need to be authorized in order to get Configure System');
          //redirecting home page
          return res.redirect('/');
        }})
      });

      //handle post setConfiguration
      app.post("/setConfiguration",(req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(!ans){//cookie is incorrect or expired
          //clearing cookie
          res.clearCookie('token');
          //set an alert
          req.flash('danger','You need to Login in order to configure the system');
          //redirect home page
          return res.redirect('/');
        }
        let username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting username out of cookie
        let responsePromise = checkUserAuth(username); //check user auth to device
        console.log("after check...");
        responsePromise.then((val) =>{
          if(val){ //user have auth to device
            let serverIP = req.body.serverIP //extracting server ip from request
            let managedIface = req.body.managedIface //extracting managedIface from request
            let monIface = req.body.monIface //extracting monIface from request
            let routerMac = req.body.routerMac //extracting routerMac from request
            let essid = req.body.essid //extracting essid from request


            console.log(serverIP)
            console.log(managedIface)
            console.log(monIface)
            console.log(routerMac)
            console.log(essid)


            data = readConfFile();
            data = updatedData(serverIP,managedIface,monIface,routerMac,essid);
            writeIntoConfFile(data)
            //set an alert
            req.flash('success','changed configurations successfully');
            //redirect home page
            return res.redirect('/');
          }
          else{ //user doesnt have auth
            //set an alert
            req.flash('danger','You need to be authorized in order to start a scan');
            //redirect home page
            return res.redirect('/');
          }
        })
      })

      //handle get Logout
      app.get("/Logout",(req,res) =>{
        //verifing cookie
        ans = verifyToken(req);
        if(ans){ //cookie is ok
          username = jwt.verify(req.cookies.token,KEY_FOR_JWT).username; //extracting username out of cookie
          msg = "15" + getMac() + padNum(username.length) + username; //creat disconnect request
          console.log(`msg: ${msg}`);
          sendServer(msg); //send server disconnect request
          res.clearCookie('token'); //clearing cookie
        }
        //set an alert
        req.flash('success','Logout successfully');
        //redirect home page
        res.redirect("/");
      });


      //starts listening to requests
      let port = 3000;
      app.listen(port,() => {
        console.log(`Started Listening  on port: ${port}`);
      });
  });
}


//helping functions

/*
this func will write the username into configuration file
input:username
output:none
*/
function setUsernameInConf(username){
  let data = readConfFile() //reading configuration file
  data = data.replace("\r","")
  lines = data.split("\n"); //spliting the lines
  for(let line in lines){ //itarete the lines
    if(lines[line].includes("username")){ //replace username line
      lines[line] = "username:*:" + username;
    }
    splited_line = []
  }
  data = lines.join("\n"); //connect the lines
  writeIntoConfFile(data) //write data to configuration file
}

/*
this func will verify the token of the request
input:request
output: true if valid cookie else false
*/
function verifyToken(req){
  if(req.cookies.token !== undefined){ //check if there is a cookie
    let token = req.cookies.token; //get the cookie
    let original = jwt.verify(token,KEY_FOR_JWT); //decode the cookie using the secret key
    console.log("json original: ", original);
    if(original.username !== undefined){ //check if there is a field of username
      return true; //case there is return valid cookie
    }
  }
  //any bad cookie or lack of cookie will return false
  return false;
}


/*
this func will send and get a response from the big server through the linking thread
input: msg to send server
output: server response
*/
async function getAnsFromServer(msg){
  return new Promise((resolve, reject) =>{
    client = new net.Socket(); //creat new socket
    let ip = "127.0.0.1"; //ip of the linking thread
    let port = "15963" //port of the linking thread
    client.connect(15963, '127.0.0.1', function() { //connect linking thread
    });
    client.write(msg,'binary'); //send linking thread msg

    client.on('data', function(data) {
      resolve(data.toString()); //return the server response through a promise
      client.destroy(); // kill client after server's response
    });

    client.on('close', function() {
    })
  })
}

/*
this func will send the server msg through the linking thread
input:msg
output:none
*/
function sendServer(msg){
  client = new net.Socket(); //creat new socket
  let ip = "127.0.0.1"; //ip of linking thread
  let port = "15963"; //port of linking thread
  client.connect(15963, '127.0.0.1', function() {
  });
  client.write(msg,'binary'); //sending msg

  client.on('data', function(data) {
    client.destroy(); // kill client after server's response
  });

  client.on('close', function() {
  });
}

/*
this func will process the internal log (csv file)
input: data of the csv file
output: attacks object
*/
function processSavedLog(data){
  stracture = { //helping stracture of the way the csv is written
    attackCode:0,
    date:1,
    hour:2,
    attacker:3
  }
  attacks = [] //attacks list
  splited_line = []
  data = data.replace("\r","")
  lines = data.split("\n"); //spliting data into lines
  console.log("lines")
  console.log(lines)
  for(let lineIndex in lines){ //itarete lines
    if(lines[lineIndex] != ''){
      splited_line = lines[lineIndex].split(","); //spliting the csv line
      //push new attack dictionary
      attacks.push({
        [splited_line[stracture.attackCode]] :{
          attacker:splited_line[stracture.attacker],
          day:splited_line[stracture.date],
          hour:splited_line[stracture.hour]
        }
      })
    }
  }
  console.log("attacks")
  console.log(attacks)
  return attacks
}

/*
this func will read the configuration file
input:none
output:csv file data
*/
function readConfFile()
{
  filePath = path.join(".", 'configuration.txt');
  data = fs.readFileSync(filePath, {encoding: 'utf-8'})
  return data;
}

/*
this func will write the configuration file
input:csv file data
output:none
*/
function writeIntoConfFile(data){
  filePath = path.join(".", 'configuration.txt');
  fs.writeFileSync(filePath, data);
}

/*
this func will update the configuration file with the data that the user supplied
input: the data the user entered
output: new Configurations file data
*/
function updatedData(serverIP,managedIface,monIface,routerMac,essid){
  data = readConfFile()
  data = data.replace("\r","")
  lines = data.split("\n"); //split to lines
  //itarete the lines
  for(let lineIndex in lines){
    if(lines[lineIndex] != ''){
      splited_line = lines[lineIndex].split(":*:"); //spliting line by seperetor
      if (splited_line[0] === "essid" && essid != ""){ //case essid line
        splited_line[1] = essid; //replace with new data
        lines[lineIndex] = splited_line[0] + ":*:" + splited_line[1] //override line with new one
      }
      else if(splited_line[0] === "monitor" && monIface != ""){ //case monitor iface line
        splited_line[1] = monIface; //replace with new data
        lines[lineIndex] = splited_line[0] + ":*:" + splited_line[1] //override line with new one
      }
      else if(splited_line[0] === "managed" && managedIface != ""){ //case managed iface line
        splited_line[1] = monIface; //replace with new data
        lines[lineIndex] = splited_line[0] + ":*:" + splited_line[1] //override line with new one
      }
      else if(splited_line[0] === "mac" && routerMac != ""){ //case router mac line
        splited_line[1] = monIface; //replace with new data
        lines[lineIndex] = splited_line[0] + ":*:" + splited_line[1] //override line with new one
      }
      else if(splited_line[0] === "serverIP" && serverIP != ""){ //case server ip line
        splited_line[1] = monIface; //replace with new data
        lines[lineIndex] = splited_line[0] + ":*:" + splited_line[1] //override line with new one
      }
    }
  }
  data = lines.join("\n");
  console.log("UPDATED DATA:")
  console.log(data);
  return data;
}

/*
this function will take the scan information and process it into
a give log msg
input: scan information (username,scanName,productMac,date,routerMac,results)
output:sturcured msg
*/
function processScanString(username,scanName,productMac,date,routerMac,results){
  var msg = "05"+productMac+padNum(username.length)+username+padNum(scanName.length)+scanName+date+routerMac+padNum((Object.keys(results)).length); //start build a request
  //var msg = "05"+productMac+padNum(username.length)+username+padNum(scanName.length)+scanName+date+"FF-FF-FF-FF-FF-FF"+padNum((Object.keys(results)).length);
  //itarete the results and add the to the msg
  for(let index=0; index < results.length; index++) {
    msg += padNum(results[index].holeName.length) + results[index].holeName + results[index].attacker + results[index].date + results[index].hour;
  }
  console.log("Scan string:")
  console.log(msg);
  return msg;
}


/*
this func will connect the main script with a path and return its response in json format
input:path to connect the main script
output:main script response
*/
async function connectMainScript(path){
  return new Promise((resolve, reject) =>{
    request({
      url: 'http://127.0.0.1:5000'+path,
      json: true
    }, function(error, response, body) {
        resolve(body);
      });
    });
}

/*
this function will check the user authentication level to the device and return true if he has authorization to it
input:username to check
output:true if the user have auth and false if he doesnt
*/
async function checkUserAuth(username){
  return new Promise((resolve, reject) =>{
    msg = "19" + getMac() + padNum(username.length) + username; //check auth msg
    client = new net.Socket(); //creat new socket
    let ip = "127.0.0.1"; //linking thread ip
    let port = "15963" //linking thread port
    client.connect(15963, '127.0.0.1', function() {
    });
    //send the msg
    client.write(msg,'binary');

    client.on('data', function(data) {
      //return answer
      resolve(parseInt(data.toString()[2])>0);
      client.destroy(); // kill client after server's response
    });

    client.on('close', function() {
    })
  })
}

//this func will pad num with a 0 if it has only one digit
function padNum(num){
  if (num>=0 && num<=9){
    return "0"+ num.toString();
  }
  return num.toString();
}


/*
this func will set the flags of attacks based on the configuration file
input:flags object
output:none
*/
function setFlags(flags){
  var splited_line = []
  data = readConfFile() //reading configuration file
  lines = data.split("\n"); //spliting it by lines
  //itarete lines
  for(let line in lines){
    if(lines[line].includes("_flag")){ //see if line is flag
      //spliting the line
      splited_line = lines[line].replace("\n","").replace("_flag","").split(":*:");
      if(splited_line[0] == "mitm"){ //check if its mitm flag
        flags.man_in_the_middle_flag = (splited_line[1] == '1') //update flag
      }
      if(splited_line[0] == "dhcp-spoofing"){ //check if its dhcp spoofing flag
        flags.dhcp_spoofing_flag = (splited_line[1] == '1') //update flag
      }
      if(splited_line[0] == "dns-spoofing"){ //check if its dns spoofing flag
        flags.dns_spoofing_flag = (splited_line[1] == '1') //update flag
      }
      if(splited_line[0] == "Evil-Twin"){ //check if evil twin flag
        flags.evil_twin_flag = (splited_line[1] == '1') //update flag
      }
      if(splited_line[0] == "Smurf"){ //check if smurf flag
        flags.smurf_flag = (splited_line[1] == '1') //update flag
      }
      if(splited_line[0] == "Syn-Flood"){ //check if syn flag
        flags.syn_flood_flag = (splited_line[1] == '1') //update flag
      }
    }
    splited_line = [] //reset spleted line
  }
}

/*
this func will return the managed interface from configuration file
input:none
output:managed iface
*/
function getManaged(){
  data = readConfFile() //reading configur file
  lines = data.split("\n"); //spliting the lines
  for(let line in lines){ //itarete lines
    if(lines[line].includes("managed")){ //case its managed iface line
      splited_line = lines[line].replace("\n","").split(":*:"); //spliting the line by seperator
      return splited_line[1]; //return iface
    }
    splited_line = [] //reset splited line
  }
  return "0";
}

/*
this func will return the essid from configuration file
input:none
output:essid
*/
function getEssid(){
  data = readConfFile() //reading configur file
  lines = data.split("\n"); //spliting the lines
  for(let line in lines){ //itarete lines
    if(lines[line].includes("essid")){ //case essid line
      splited_line = lines[line].replace("\n","").split(":*:"); //spliting the line by seperator
      return splited_line[1]; //returrn essid
    }
    splited_line = [] //reset splited line
  }
  return "0";
}

/*
this func will return the router mac from configuration file
input:none
output:router mac
*/
function getRoterMac(){
  data = readConfFile() //reading configur file
  lines = data.split("\n"); //spliting the lines
  for(let line in lines){ //itarete lines
    if(lines[line].includes("mac")){ //case router mac line
      splited_line = lines[line].replace("\n","").split(":*:"); //spliting the line
      return splited_line[1]; //return router mac
    }
    splited_line = [] //reset splited line
  }
  return "0";
}

/*
this func will return the monitor iface from configuration file
input:none
output:monitor iface
*/
function getMonIface(){
  data = readConfFile() //reading configur file
  lines = data.split("\n"); //spliting the lines
  for(let line in lines){ //itarete lines
    if(lines[line].includes("monitor")){ //case monitor iface line
      splited_line = lines[line].replace("\n","").split(":*:"); //spliting the line
      return splited_line[1]; // return monitor iface
    }
    splited_line = [] //reset splited line
  }
  return "0";
}

/*
this func will return the server ip from configuration file
input:none
output:server ip
*/
function getServerIP(){
  data = readConfFile() //reading configur file
  lines = data.split("\n"); //spliting the lines
  for(let line in lines){ //itarete lines
    if(lines[line].includes("serverIP")){ //case server ip line
      splited_line = lines[line].replace("\n","").split(":*:"); //spliting line
      return splited_line[1]; //return server ip
    }
    splited_line = [] //reset splited line
  }
  return "0";
}

/*
this func will return the device mac
input:none
output:server ip
*/
function getMac(){
  let mac = (os.networkInterfaces()[getManaged()][0].mac); //system call to get the mac
  mac = mac.replace(/:/g,"-"); //replaceing : to -
  return mac.toUpperCase();
}

/*
this func will get the server response (one long string) and process it into scans list
input: server response
output:scans list
*/
function handleFilesString(str)
{
  console.log(str);
  //setting variables
  let id,nameLen,name,date,productMac,routerMac,usernameLen,username;
  files = [] //scans list
  while(str !== ""){
    id = str.substring(0,32); //taking scan id
    str = str.replace(str.substring(0,32),""); //cutting the id from string
    nameLen = parseInt(str.substring(0,2)); //taking scan name len
    str = str.replace(str.substring(0,2),""); //cutting scan name len from string
    name = str.substring(0,nameLen); //taking scan name
    str = str.replace(str.substring(0,nameLen),""); //cutting scan name
    date = str.substring(0,10); //taking scan date
    str = str.replace(str.substring(0,10),""); //cutting scan date from string
    productMac = str.substring(0,17); //taking product mac
    str = str.replace(str.substring(0,17),""); //cutting product mac form string
    routerMac = str.substring(0,17); //taking router mac
    str = str.replace(str.substring(0,17),""); //cutting router mac from string
    usernameLen = parseInt(str.substring(0,2)); //taking username len
    str = str.replace(str.substring(0,2),""); //cutting username len from string
    username = str.substring(0,usernameLen); //taking username
    str = str.replace(str.substring(0,usernameLen),""); //cutting username from string
    files.push({ //push scan object to scan list
      id:id,
      name:name,
      date:date,
      productMac:productMac,
      routerMac:routerMac,
      username:username
    })
  }
  console.log(JSON.stringify(files));
  return files;
}

/*
this func will get the server response (one long string) and process it into scan object
input: server response
output:scans list
*/
function handleFileString(str,key){
  console.log("key")
  console.log(key)
  console.log("key type")
  console.log(typeof(key))
  var scan = {results:[]} //creat scan object with empty list of attacks
  var len = 0;
  let holeName,holeExplain,wayToSolve,date,hour,attacker,status; //init variables
  len = parseInt(str.substring(0,2)); //taking len of user who took the scan
  str = str.replace(str.substring(0,2),""); //cutting len of user who took the scan
  scan.username = str.substring(0,len); //taking the user who took the scan
  str = str.replace(str.substring(0,len),""); //cutting the user who took the scan
  scan.productMac = str.substring(0,17); //taking the productMac
  str = str.replace(str.substring(0,17),""); //cutting the productMac
  len = parseInt(str.substring(0,2)); //taking scan name len
  str = str.replace(str.substring(0,2),""); //cutting scan name len
  scan.name = str.substring(0,len); //taking scan name
  str = str.replace(str.substring(0,len),""); //cutting scan name
  scan.date = str.substring(0,10); //taking scan date
  str = str.replace(str.substring(0,10),""); //cutting scan date
  scan.routerMac = str.substring(0,17); //taking router mac
  str = str.replace(str.substring(0,17),""); //cutting router mac
  scan.key = key; //set the key to scan
  while(str !== ""){
    len = parseInt(str.substring(0,2)); //taking attack name len
    str = str.replace(str.substring(0,2),""); //cutting attack name len
    holeName = str.substring(0,len); //taking attack name
    str = str.replace(str.substring(0,len),""); //cutting attack name
    date = str.substring(0,10); //taking the date of the attack
    str = str.replace(str.substring(0,10),""); //cutting the date of the attack
    hour = str.substring(0,8); //taking attack hour
    str = str.replace(str.substring(0,8),""); //cutting attack hour
    attacker = str.substring(0,17); //taking attacker
    str = str.replace(str.substring(0,17),""); //cutting attacker
    scan.results.push({ //push attack object into scan
      holeName:holeName,
      holeExplain:holeExplain,
      wayToSolve:wayToSolve,
      date:date,
      hour:hour,
      attacker:attacker
    })
  }
  console.log(JSON.stringify(scan));
  return scan;
}
