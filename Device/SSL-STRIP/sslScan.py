from scapy.all import UDP,TCP,IP,sniff


req = []
sslStripFlag = False

def PacketHandler(packet) :
    if packet.haslayer(UDP) or packet.haslayer(TCP):
        if packet.haslayer(UDP):
            if packet[UDP].dport == 443:
                req.append({packet[IP].src:packet[IP].dst})
            elif packet[UDP].sport == 443 or packet[UDP].sport == 80:
                for cell in req:
                    for key in cell.keys():
                        if key == packet[IP].dst and cell[key] == packet[IP].src:
                            flag = True
                    if flag:
                        if packet[UDP].sport == 80:
                            # TODO: send report
                            print("ssl-strip occured")
                        req.remove(cell)
                    flag = False

def sslStripScan():
    print("SSL-STRIP Scan started...")
    sniff(iface="wlp2s0", prn=PacketHandler)

if(__name__ == '__main__'):
    sslStripScan()
