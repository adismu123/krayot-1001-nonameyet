import socket
import hashlib

bind_ip = '127.0.0.1'
bind_port = 9999


def sendOK():
    ip = "127.0.0.1"
    port = 4432

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip,port))
    sock.send("ok".encode("ascii"))
    sock.close()


def handle_client_connection():
    go = False
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((bind_ip, bind_port))
    print('Listening on {} {}'.format(bind_ip, bind_port))

    while(not go):
        sock.listen(10)  # max backlog of connections
        print(1)
        client_socket, address = sock.accept()
        print('Accepted connection from {} {}'.format(address[0], address[1]))
        msg = client_socket.recv(32)
        msg = msg.decode('utf-8')

        print(msg)
        print(md5Str)

        if md5Str == msg:
            client_socket.send("True".encode('utf-8'))
        else:
            client_socket.send("False".encode('utf-8'))
        client_socket.close()
        print("socket close")


input("Hey Network Administrator! \nIn order to start testing, you have to authenticate yourself."
      " \nPlease open port " + str(bind_port) + " on your router and press Enter.")
verifyStr = input("In order to verify your identity, please enter a string here:\n")
md5Str = hashlib.md5(verifyStr.encode('utf-8')).hexdigest()
#sendOK()

handle_client_connection()
print ("prog doen")
