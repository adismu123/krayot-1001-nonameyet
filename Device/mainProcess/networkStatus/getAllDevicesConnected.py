#!/usr/bin/env python3

import sys
import requests #pip install needed!
import json
from scapy.all import ARP, Ether, conf, srp
import subprocess

# network scanner moduls:
from networkStatus.checkMacAddr import chack_mac_addr
#import devices
devices=[]
#pip install requests

w = '\033[37m'
g = '\033[32m'
b = '\033[34m'
y = '\033[33m'
c = '\033[36m'

def show_identity_and_valid_mac(pkt):
   metadata = requests.get('http://macvendors.co/api/{}'.format(pkt[Ether].src)).json()['result']
   if('error' not in metadata.keys()):
       print (w+"{mac} - {ip} ({company})".format(mac=pkt[Ether].src, ip=pkt[ARP].psrc, company=metadata['company']))
       device_deatiles = { 'ip' : pkt[ARP].psrc , 'mac' : pkt[Ether].src.replace(":","-").upper() , 'company' : metadata['company'] , 'is_valid' : chack_mac_addr(pkt[Ether].src.replace(":","-").upper(),username) }
       return device_deatiles
   else:
       #print (w+"{mac} - {ip} ({company})".format(mac=pkt[Ether].src, ip=pkt[ARP].psrc, company=metadata['company']))
       device_deatiles = { 'ip' : pkt[ARP].psrc , 'mac' : pkt[Ether].src.replace(":","-").upper() , 'company' : "Unknown Manufacturer" , 'is_valid' : chack_mac_addr(pkt[Ether].src.replace(":","-").upper(),username) }
       return device_deatiles


def get_all_devices(_username):
    global username
    #if we are holding a list of all the devices, there is no reason to scan
    if len(devices) > 0:
        #TODO: CHECK IF SOMEONE HAS DISCONNECTED
        return json.dumps(devices)

    username = _username
    cidr = subprocess.run("ip -o -f inet addr show | awk '/scope global/ {print $4}'", shell=True, stdout=subprocess.PIPE, universal_newlines=True).stdout
    devices_connected=[]
    print (c+"[*] scanning...")
    answers, _ = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst = cidr), timeout = 2, inter = 0.1)

    print (g+"[*] scan completed"+w+"\n\nDevices on network")
    for _, ans in answers:
        device_deatiles = show_identity_and_valid_mac(ans)
        #print (device_deatiles)
        devices_connected.append(device_deatiles)
    return json.dumps(devices_connected)


if __name__ == '__main__':
    get_all_devices("defultUserName")
