#!/usr/bin/env python3

# python modules
import sys
import os
import base64
import socket
import subprocess
import gi
import socket
import hashlib
import datetime
import csv
import netifaces
from threading import Thread
import time

# network scanner modules
from authenticate.clientAuth import get_authentication
from sniffing import startSniffing
from connectDeviceToServer.ConnectDeviceToServer import *
from networkStatus.getAllDevicesConnected import *
from flask import Flask,jsonify,request
from scapy.all import ARP,Ether,srp1
from sendMail.sendMail import send_mail
from readFromConfiguration import *
from getPasswordStrength import password_check
from setUp.flaskServer import main as setUp
from scans.dhcpScan import setKnownAddresses as dhcp_setKnownAddresses
from scans.evilTwinScan import set_parms as evil_twin_set_paems
from scans.synFloodingScan import set_username as syn_flood_set_username

from sniffing import man_in_the_middle_flag,dhcp_spoofing_flag,dns_spoofing_flag,evil_twin_flag,set_flags

SERVER_IP = getServerIp() #"192.168.43.11"

app = Flask(__name__)
devices = {}

def getPass():
    """
    this func will read the password that stored in the conf file
    input:none
    output: network password
    """
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for i in range(len(lines)):
    	splited_line = lines[i].replace("\n","").split(":*:")
    	if splited_line[0] == "sod":
    		return base64.b64decode(splited_line[1].encode("utf-8")).decode("utf-8")
    	splited_line = []



@app.route("/routerMac")
def returnMac():
    """
    this func will return the router mac
    input:none
    output:router Mac
    """
    mac = getMac()
    return jsonify(res = mac)

@app.route("/auth/<authString>")
def auth(authString):
    """
    this func will make the authentication in front of the network manager
    input:auth string
    output: true in case of ok and false otherwise
    """
    #is_authenticate = get_authentication(authString)
    #if is_authenticate == "True":
    #    # authenticated
    #    return jsonify(res = "True")
    #else:
    #    # not authenticated
    #    return jsonify(res = "False")
    return jsonify(res = "True")

@app.route("/changeScan/<scan_name>")
def changeScan(scan_name):
    """
    this func will change scan flag
    input:scan flag name
    output: 200 in case of success
    """
    if scan_name == "dhcp-spoofing":
        dhcp_setKnownAddresses()
    if scan_name == "Evil-Twin":
        evil_twin_set_paems()
    if scan_name == "Syn-Flood":
        syn_flood_set_username()
    splited_line = []
    status = "1"
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    print("changeScan read file")
    for i in range(len(lines)):
        if "_flag" in lines[i] and scan_name in lines[i]:
            splited_line = lines[i].replace("\n","").split(":*:")
            print("splited_line[0]: ",splited_line[0],"\tsplited_line[1]: ",splited_line[1])
            if splited_line[1] == "1":
                status = "0"
            lines[i] = splited_line[0]+":*:"+status+"\n"
            print("scan changed into status: ",status)
        splited_line = []
    f = open(r"./configuration.txt","w")
    f.writelines(lines)
    f.close()
    print("changeScan write lines into file ")
    set_flags()
    return jsonify(res = "200")


@app.route("/viewNetwork/<username>")
def viewNetwork(username):
    """
    this function will return the network current state
    input: username of the user who ask it
    output: network current state
    """
    return jsonify(res = get_all_devices(username))

@app.route("/filterLog")
def filterLog():
    """
    this func will filter the saved log file so attackes wont apeare several times
    input:none
    output: true in case of success and false otherwise
    """
    # log file positiones
    ATTACK_CODE = 0
    DATE = 1
    TIME = 2
    ATTACKER = 3

    # read the log:
    with open(r'./temp_log.csv', 'r') as csvFile:
        lines = csv.reader(csvFile)
        events = list(lines)
        events.append("end-of-file")
        print("filtering log. list len:", len(events))
        # filter the log only if there is more then one event
        if len(events) > 2:
            i = 0
            j = 1
            while events[j] != "end-of-file":
                if events[i][ATTACK_CODE] == events[j][ATTACK_CODE] and events[i][DATE] == events[j][DATE] and events[i][ATTACKER] == events[j][ATTACKER]:
                    if int(events[j][TIME][3:5]) - int(events[i][TIME][3:5]) < 2:
                        events.remove(events[j])
                        i -= 1
                        j -= 1
                i += 1
                j += 1
            events.remove("end-of-file")
            # write the results back to the file:
            with open(r'./temp_log.csv', 'w') as csvFile:
                for event in events:
                    writer = csv.writer(csvFile)
                    writer.writerow(event)
            csvFile.close()
            return jsonify(res = "True")
        elif len(events) == 2:
            #if it goes here, it means that there is only one event. no filtering needed
            return jsonify(res = "True")
        else:
            # the events list contains only the end-of-file and there is no attack!
            return jsonify(res = "False")



@app.route("/passwordGrade")
def getPassStrength():
    """
    this func will return the network's password grade
    input:none
    output:the network password grade
    """
    print(password_check(getPass()))
    return jsonify(res = password_check(getPass()))

@app.route("/sendLog/<logString>")
def sendLog(logString):
    """
    this func will send the server the log file
    input:log string in base64 format
    output:server respose
    """
    logString = base64.b64decode(logString)
    ans = getAnsFromServer(logString)
    return jsonify(res = ans)

@app.route("/sendLogEmail", methods=['GET'])
def sendLogEmail():
    """
    this func will send the log in mail
    input:html file in base64 format, username, and device mac
    output: 200 in case of OK and 400 in case of Error
    """
    try:
        print(type(request.args.get('html')))
        htmlString = base64.b64decode(request.args.get('html')).decode("utf-8")
        print(htmlString)
        print(type(htmlString))
        user = request.args.get('user')
        print(user)
        mac = request.args.get('mac')
        print(mac)
        f = open("log.html","w")
        f.write(htmlString)
        f.close()
        emails = askMail(mac,user)
        send_from = "networkscanner1@gmail.com"
        subject = "Log File"
        text = user + " has requested the log:"
        files = ["log.html"]
        for mail in emails:
            send_mail(send_from,mail,subject,text,files)
        if os.path.exists("log.html"):
            os.remove("log.html")
        return jsonify(res = "200")
    except Exception as e:
        print(e)
        return jsonify(res = "400")


def askMail(mac,user):
    """
    this func will ask the emails of the admins of the device
    input: mac of device and the username that asks to send the mail
    output: list of emails
    """
    length = 0
    times = 0
    mail = ""
    emails = []
    msg = "23" + mac + str(len(user)).zfill(2) + user
    res = getAnsFromServer(msg.encode("utf-8"))
    type = res[0:3]
    res = res[3:]
    print("res: ",res)
    if type == "24C":
        times = int(res[0:2])
        res = res[2:]
        print("times: ",times)
        print("res: ",res)
        for i in range(times):
            length = int(res[0:2])
            print("length: ",length)
            res = res[2:]
            print("res: ",res)
            mail = res[0:length]
            print("mail: ",mail)
            res = res[length:]
            print("res: ",res)
            emails.append(mail)
    return emails

def shut_down_flags():
    """
    this func will turn off all the scans flags
    input:none
    output:none
    """
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    print("changeScan read file")
    for i in range(len(lines)):
        if "_flag" in lines[i]:
            splited_line = lines[i].replace("\n","").split(":*:")
            lines[i] = splited_line[0]+":*:"+"0"+"\n"
        splited_line = []
    f = open(r"./configuration.txt","w")
    f.writelines(lines)
    f.close()


def getAnsFromServer(msg):
    """
    this func will send msg to the server and return the response
    input: msg to send the server
    output: server response
    """
    print('msg:')
    print(msg)
    print('type:')
    print(type(msg))
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Connect the socket to the port where the server is listening
    server_address = ('127.0.0.1', 15963)
    sock.connect(server_address)
    sock.sendall(msg)
    res = sock.recv(4096)
    sock.close()
    return res.decode("utf-8")

if __name__ == '__main__':
    # initialization:
    shut_down_flags()
    set_flags()
    #Add Noam connecting to network using AP
    setUp()

    #wait for the user to enter authenticate authString
    router_essid = getESSID()
    router_bssid = getMac()
    print("routerMac: ",router_bssid)
    print("routerEssid: ",router_essid)

    #running website
    os.system("xterm -hold -e 'node ../../WebSite/app.js'&")
    print("server run")

    time.sleep(3)
    #lunch the program that link the device to server
    linkThread = Thread(target=startConnectingHttpToServer, args=(SERVER_IP, 5656))
    linkThread.start()
    sniffingThread = Thread(target=startSniffing, args=(router_essid,router_bssid))
    sniffingThread.start()
    app.run()
