#!/usr/bin/env python3


def getMac():
    flag = False
    gws=netifaces.gateways()
    ip,iface = gws['default'][netifaces.AF_INET]
    while flag:
        ans = srp1(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=ip),iface=getManaged(),inter=0.2,timeout=0.5)
        try:
            if ans[ARP].hwsrc.replace(":","-").upper() != None:
                if ans[ARP].src == ip:
                    return ans[ARP].hwsrc.replace(":","-").upper()
        except Exception as e:
            return "-----------------"

def getESSID():
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        splited_line = line.split(":*:")
        if splited_line[0] == "essid":
            print("ESSID is: ",splited_line[1])
            return splited_line[1]
        splited_line = []

def getMonitor():
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        line = line.replace("\n","")
        splited_line = line.split(":*:")
        if splited_line[0] == "monitor":
            print("monitor: ",splited_line[1])
            return splited_line[1]
        splited_line = []

def getManaged():
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        line = line.replace("\n","")
        splited_line = line.split(":*:")
        if splited_line[0] == "managed":
            print("managed: ",splited_line[1])
            return splited_line[1]
        splited_line = []

def getServerIp():
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        line = line.replace("\n","")
        splited_line = line.split(":*:")
        if splited_line[0] == "serverIP":
            print("server ip: ",splited_line[1])
            return splited_line[1]
        splited_line = []

def getUsername():
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        if "username" in line:
            line = line.replace("\n","")
            splited_line = line.split(":*:")
            return splited_line[1]

def getDHCP():
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        splited_line = line.split(":*:")
        if splited_line[0] == "dhcp":
            print("DHCP is: ",splited_line[1])
            return splited_line[1][:-1]
        splited_line = []


def getDNS():
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        splited_line = line.split(":*:")
        if splited_line[0] == "dns":
            print("DNS is: ",splited_line[1])
            return splited_line[1][:-1]
        splited_line = []

def getRouterIP():
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        splited_line = line.split(":*:")
        if splited_line[0] == "routerIP":
            print("router ip is: ",splited_line[1])
            return splited_line[1][:-1]
        splited_line = []
