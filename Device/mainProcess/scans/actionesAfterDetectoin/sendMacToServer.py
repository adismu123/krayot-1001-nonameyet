import socket
import netifaces

from readFromConfiguration import *


def getMonitorr():
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        line = line.replace("\n","")
        splited_line = line.split(":*:")
        if splited_line[0] == "monitor":
            print("monitor: ",splited_line[1])
            return splited_line[1]
        splited_line = []

def getManagedd():
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        line = line.replace("\n","")
        splited_line = line.split(":*:")
        if splited_line[0] == "managed":
            print("managed: ",splited_line[1])
            return splited_line[1]
        splited_line = []



port = 15963
INTERFACE = getManaged()

def send_mac_to_server(username, macToSend):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ip = "127.0.0.1"
    server_address = (ip, port)
    sock.connect(server_address)

    my_mac = netifaces.ifaddresses(INTERFACE)[netifaces.AF_LINK][0]['addr'].replace(":","-").upper()

    # 17 + ProductMacAdrress + UNlen + UN + MAC ADDRESS
    msg = "17" + my_mac + str(len(username)).zfill(2) + username + macToSend
    sock.sendall(msg.encode("ascii"))
    ans = sock.recv(4096).decode("utf-8")
    print(ans)
    if(ans[-1] == 'C'):
        return '0'
    else:   # ans[-1] == 'D'
        print("mac regiserition denied")
        return '1'  # mac denied
