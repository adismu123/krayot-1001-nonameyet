#!/usr/bin/env python3


from __future__ import print_function
from scapy.all import *
import time
import csv
from threading import Thread
import datetime
from readFromConfiguration import getDNS, getDHCP, getRouterIP, getManaged, getMac, getUsername
from scans.actionesAfterDetectoin.sendMacToServer import send_mac_to_server
from scans.actionesAfterDetectoin.deauthentication import deauth,deauthVictim

ATTACK_CODE = "DHCP-SPOOFING"

dns = None
dhcp = None
router = None
managed_iface = None
router_mac = None
username = None

"""
this func will set the knowen parameters from the configuration file
input:none
output:none
"""
def setKnownAddresses():
    global dns, dhcp, router, managed_iface,router_mac,username
    dns = getDNS()
    dhcp = getDHCP()
    router = getRouterIP()
    managed_iface = getManaged()
    router_mac = getMac()
    print("set known address: router mac - ", router_mac)
    username = getUsername()


"""
this func will write the log file an event
input:attacker mac
output:none
"""
def addEvent(mac):
    global username
    time = datetime.datetime.now()
    #write to file
    event = [ATTACK_CODE, time.strftime("%d/%m/%Y"), time.strftime("%H:%M:%S"), mac]
    with open(r'./temp_log.csv', 'a') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(event)
    csvFile.close()


"""
this func will return the mac of an ip using arp request
if there isnt answer in 0.8 sec it returns -----------------
input:ip
output:mac of the given ip
"""
def getMacByIp(ip):
    flag = True
    while flag:
        ans = srp1(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=ip),iface=getManaged(),inter=0.2,timeout=0.8)
        try:
            if ans[ARP].hwsrc.replace(":","-").upper() != None:
                if ans[ARP].src == ip:
                    return ans[ARP].hwsrc.replace(":","-").upper()
        except Exception as e:
            return "-----------------"

"""
this func will check the dhcp Information to our known Information and
activet deauth and send mac to server
input:dhcp packet Information
output:none
"""
def checkStats(dnsIP,routerIP,dhcpIP,senderMAC,victimMAC):

    #####################################################
    #   init                                            #
    #####################################################
    global dns, dhcp, router, router_mac    #the global variabls are the known addresses
    dns_spoofed_mac = ""
    router_spoofed_mac = ""
    dhcp_spoofed_mac = ""
    sender = ""
    deauthenticated = []

    flag = False #flag in order to trigger deauth on victim in case of attack

    #####################################################
    #                     Checks                        #
    #####################################################

    """check sender"""
    sender = senderMAC
    print("sender:",sender)
    print("router_mac:",router_mac)
    if sender.replace(":","-").upper() != router_mac: # check if sender isnt the router
        flag = True
        if sender.replace(":","-").upper() != "-----------------": #check if sender is known
            print(sender)
            print(router_mac)
            print("Bad sender >>> got:\t" + sender + "insted of:\t" + router_mac)
            addEvent(sender.replace(":","-").upper())
            deauthenticated.append(sender) #add to list of deauth devices
        else:
            addEvent("Unknown Attacker.")

    print("real:",dns)
    print("got:",dnsIP)
    """check dns"""
    if dnsIP != dns: #check if the dns isnt the real dns
        flag = True
        dns_spoofed_mac = getMacByIp(dnsIP) #tring to get the mac of the dns ip
        if dns_spoofed_mac != "-----------------": #ceck if attacker is known
            if dns_spoofed_mac not in deauthenticated: #check if the attacker isnt allready in the deauth devices list
                if dns_spoofed_mac.replace(":","-").upper() != router_mac and dns_spoofed_mac.replace(":","-").upper() != "FF-FF-FF-FF-FF-FF": #check if the attacker is not the router
                    print("Bad dns >>> got:\t" + dnsIP + "insted of:\t" + dns)
                    addEvent(dns_spoofed_mac.replace(":","-").upper())
                    deauthenticated.append(dns_spoofed_mac)#add to list of deauth devices
        else:
            print("dhcp got bad dns but attacker wasnt found")
            addEvent("Unknown Attacker.")

    """check router ip"""
    if routerIP != router: #check if got fake router ip
        flag = True
        router_spoofed_mac = getMacByIp(routerIP) #tring to get the mac of the fake router ip
        if router_spoofed_mac != "-----------------": #check if the mac is known
            if router_spoofed_mac not in deauthenticated: #check if the attacker isnt allready in the deauth devices list
                if router_spoofed_mac.replace(":","-").upper() != router_mac and router_spoofed_mac.replace(":","-").upper() != "FF-FF-FF-FF-FF-FF": #check if the attacker is not the router
                    print("Bad router >>> got:\t" + routerIP + "insted of:\t" + router)
                    addEvent(router_spoofed_mac)
                    deauthenticated.append(router_spoofed_mac) #add to list of deauth devices
        else:
            print("dhcp got bad routerIP but attacker wasnt found")
            addEvent("Unknown Attacker.")

    if dhcpIP != dhcp: #check if got fake dhcp ip
        flag = True
        dhcp_spoofed_mac = getMacByIp(dhcpIP) #tring to get the mac of the fake dhcp ip
        if dhcp_spoofed_mac != "-----------------": #check if the mac is known
            if dhcp_spoofed_mac not in deauthenticated: #check if the attacker isnt allready in the deauth devices list
                if dhcp_spoofed_mac.replace(":","-").upper() != router_mac and dhcp_spoofed_mac.replace(":","-").upper() != "FF-FF-FF-FF-FF-FF": #check if the attacker is not the router
                    print("Bad dhcp >>> got:\t" + dhcpIP + "insted of:\t" + dhcp)
                    addEvent(dhcp_spoofed_mac)
                    deauthenticated.append(dhcp_spoofed_mac) #add to list of deauth devices
        else:
            print("dhcp got bad dhcp but attacker wasnt found")
            addEvent("Unknown Attacker.")

    #####################################################
    #      Send To Server  +  deauthentication          #
    #####################################################
    print("deauthentication list:")
    print(deauthenticated)
    #iterate deauth devices list and send them to server + deauth them
    for mac_addr in deauthenticated:
        print("mac_addr:")
        print(mac_addr)
        print("type mac_addr:")
        print(type(mac_addr))
        send_mac_to_server(username, mac_addr.replace(":","-").upper())
        Thread(target=deauth, args=([mac_addr.replace(":","-").upper()])).start()

    if(flag): #if under attack
        print("deauth victim")
        Thread(target=deauthVictim, args=([victimMAC.replace(":","-").upper()])).start() #death victim to short duretion in order him to get tje correct information


""""
this function will extract dhcp_options out of dhcp packet by key
input: dhcp options segment from dhcp packet
output: requested key or none in case of error
"""
def get_option(dhcp_options, key):

    must_decode = ['hostname', 'domain', 'vendor_class_id']
    try:
        for i in dhcp_options:
            if i[0] == key:
                # If DHCP Server Returned multiple name servers
                # return all as comma seperated string.
                if key == 'name_server' and len(i) > 2:
                    return ",".join(i[1:])
                # domain and hostname are binary strings,
                # decode to unicode string before returning
                elif key in must_decode:
                    return i[1].decode()
                else:
                    return i[1]
    except:
        pass

"""
this func will handle dhcp packet
input:packet
output:none
"""
def handle_dhcp_packet(packet):
    if packet.haslayer(DHCP):
        # check if DHCP offer
        if DHCP in packet and packet[DHCP].options[0][1] == 2:
            print('---')
            print('New DHCP Offer')
            #print(packet.summary())
            #print(ls(packet))

            #extract data out of packet
            default_getway = get_option(packet[DHCP].options, 'router')
            name_server_ip = get_option(packet[DHCP].options, 'name_server')
            dhcp_server_ip = packet[IP].src
            sender = packet[Dot11].addr2.replace(":","-").upper()

            victim = packet[Dot11].addr1.replace(":","-").upper()

            #pass to check stats to check the given data
            checkStats(name_server_ip,default_getway,dhcp_server_ip,sender,victim)

            print("DHCP Server:\t" + packet[IP].src + "\t" + packet[Dot11].addr2 )
            print("offered:\t" + packet[BOOTP].yiaddr)

            print("DHCP Options:")
            print("router:\t" + default_getway + "\n" + "name_server:\t" + name_server_ip)

        # Match DHCP ack
        elif DHCP in packet and packet[DHCP].options[0][1] == 5:
            print('---')
            print('New DHCP Ack')
            #print(packet.summary())
            #print(ls(packet))

            #extract data out of packet
            default_getway = get_option(packet[DHCP].options, 'router')
            name_server_ip = get_option(packet[DHCP].options, 'name_server')
            dhcp_server_ip = packet[IP].src
            sender = packet[Dot11].addr2.replace(":","-").upper()

            victim = packet[Dot11].addr1.replace(":","-").upper()

            #pass to check stats to check the given data
            checkStats(name_server_ip,default_getway,dhcp_server_ip,sender,victim)

            print("DHCP Server:\t" + packet[IP].src + "\t" + packet[Dot11].addr2 )
            print("offered:\t" + packet[BOOTP].yiaddr)

            print("DHCP Options:")
            print("router:\t" + default_getway + "\n" + "name_server:\t" + name_server_ip)

if __name__ == "__main__":
    setKnownAddresses()
    print("router mac:\t",getMac())
    print("test mac:\t",getMacByIp("1.1.1.1"))
    sniff(prn=handle_dhcp_packet, iface="mon0")
