#!/usr/bin/env python3

from scapy.all import Dot11, sniff,srp1,Ether,ARP
import datetime
import csv
import netifaces
from readFromConfiguration import *
from scans.actionesAfterDetectoin.sendMacToServer import send_mac_to_server

essid = None
bssid = None
username = None

ATTACK_CODE = "Evil Twin"

def set_parms():
    # the funce desigened to initialize the parms about,
    # and will be called when the attack will be activated.
    global essid, bssid, username
    essid = getESSID()
    bssid = getMac()
    username = getUsername()


def evil_twin_scan(packet):
    global essid, bssid
    # sniffing wifi packets
    if packet.haslayer(Dot11):
        # check if packet is a beacon frame
        if packet.type == 0 and packet.subtype == 8:
            # beacon frame founded
            if essid == packet.info.decode('utf-8'):
                print("essid: ",essid, "sniffed: ", packet.info.decode('utf-8'))
                # essid found
                print(bssid,packet.addr2.replace(":","-").upper())
                if bssid != packet.addr2.replace(":","-").upper():
                    action_while_evil_twin_attack(packet.addr2.replace(":","-").upper())
                    print("evil twin")

def action_while_evil_twin_attack(mac):
    global username
    time = datetime.datetime.now()

    #notify server
    send_mac_to_server(username,mac)

    #write to file
    event = [ATTACK_CODE, time.strftime("%d/%m/%Y"), time.strftime("%H:%M:%S"), mac.replace(":","-").upper()]
    with open(r'./temp_log.csv', 'a') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(event)
    csvFile.close()

if __name__ == "__main__":
    sniff(iface=getMonitor(), prn=evil_twin_scan)
