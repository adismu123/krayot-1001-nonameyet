#!/usr/bin/env python3

from scapy.all import ICMP, sniff, Dot11
import time
import datetime
import csv
from threading import Thread
from scans.actionesAfterDetectoin.sendMacToServer import send_mac_to_server
from scans.actionesAfterDetectoin.deauthentication import deauth

username=None

BIG_TIME_UNIT = 15
SMALL_TIME_UNIT = 7
ATTACK_CODE = "Smurf"
later = time.time()
icmp_list = []
icmp_dict = {}
icmp_counter = {}
first_paacket_in_list_time = None


def action_while_smurf_attack(mac):
    global username
    time = datetime.datetime.now()
    Thread(target=deauth, args=([mac])).start()

    #notify server
    send_mac_to_server(username,mac)

    #write to file
    event = [ATTACK_CODE, time.strftime("%d/%m/%Y"), time.strftime("%H:%M:%S"), mac.replace(":","-").upper()]
    with open(r'./temp_log.csv', 'a') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(event)
    csvFile.close()


def action_while_smurf_attack_with_multiple_senders():
    global username
    time = datetime.datetime.now()

    #write to file
    event = [ATTACK_CODE, time.strftime("%d/%m/%Y"), time.strftime("%H:%M:%S"), "Multiple Senders."]
    with open(r'./temp_log.csv', 'a') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(event)
    csvFile.close()



def detect_smarf_attack(packet):
    global first_paacket_in_list_time,icmp_list, icmp_dict
    currnt_packet_time = time.time()
    if packet.haslayer(ICMP):
        if  str(packet.getlayer(ICMP).type) == "8":
            sender = packet[Dot11].addr2
            if not first_paacket_in_list_time or int(currnt_packet_time - first_paacket_in_list_time) > 15:
                print("in if")
                # first packet in the list is there for while...
                # there is no suspicion to SMURF attack.

                # reset the list and the time frame:
                icmp_list = [ packet ]
                first_paacket_in_list_time = currnt_packet_time
            else:
                if len(icmp_list) >= 25:
                    print("num: ", sender )
                    if icmp_counter[ sender ] > 14:
                        # attack is beeing accure!
                        print("under smurff attac")
                        action_while_smurf_attack(sender)
                        return
                    else:
                        # attack is beeing accure!
                        print("under smurff attac")
                        action_while_smurf_attack_with_multiple_senders()
                        return

                else:
                    icmp_list.append(sender)
            try:
                print("time diff:",currnt_packet_time - icmp_dict[ sender ])
            except:
                pass
            if sender not in icmp_dict or currnt_packet_time - icmp_dict[ sender ] > 15:
                # reset icmp dictionary:
                icmp_dict[ sender ] = currnt_packet_time
                icmp_counter[ sender ] = 1
            else:
                print("num of packets:",icmp_counter[ sender ])
                if icmp_counter[ sender ] > 14:
                    # attack is beeing accure!
                    print("under smurff attac")
                    action_while_smurf_attack(sender)
                else:
                    icmp_counter[ sender ] = icmp_counter[ sender ] + 1




if __name__ == "__main__":
    sniff(prn=detect_smarf_attack,iface="mon0")
