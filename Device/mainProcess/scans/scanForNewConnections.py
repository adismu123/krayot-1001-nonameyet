#!/usr/bin/env python3

from scapy.all import ARP, Dot11, sniff
import requests
from threading import Thread
#import devices
from networkStatus.checkMacAddr import chack_mac_addr
from scans.actionesAfterDetectoin.deauthentication import deauth
devices=[] #TODO: delete this line and take out the comment above.
username = "noam" #TODO: read this!


def get_deatils(pkt):
    #TODO: disconnect if evil produckt
    print("got new device in network!", pkt[ARP].psrc)
    return
    metadata = requests.get('http://macvendors.co/api/{}'.format(pkt[Dot11].addr2)).json()['result']
    if('error' not in metadata.keys()):
       print ("{mac} - {ip} ({company})".format(mac=pkt[Dot11].addr2, ip=pkt[ARP].psrc, company=metadata['company']))
       device_deatiles = { 'ip' : pkt[ARP].psrc , 'mac' : pkt[Dot11].addr2.replace(":","-").upper() , 'company' : metadata['company'] , 'is_valid' : "1" }
       return device_deatiles
    else:
       #print (w+"{mac} - {ip} ({company})".format(mac=pkt[Dot11].addr2, ip=pkt[ARP].psrc, company=metadata['company']))
       device_deatiles = { 'ip' : pkt[ARP].psrc , 'mac' : pkt[Dot11].addr2.replace(":","-").upper() , 'company' : "ERROR" , 'is_valid' : "1" }
       return device_deatiles

def scan_arp_requests(packet):
    global devices
    if packet.haslayer(ARP):
        for device in devices:
            if device['ip'] == packet[ARP].psrc:
                return

        # ip is not in list
        if chack_mac_addr(packet[Dot11].addr2,username) == "0":
            Thread(target=deauth, args=([packet[Dot11].addr2])).start()
        devices.append(get_deatils(packet)) # must be valid mac. no need to ask the servers

if __name__ == "__main__":
    sniff(prn=scan_arp_requests, iface="mon0")
