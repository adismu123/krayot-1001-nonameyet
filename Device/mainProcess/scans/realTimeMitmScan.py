#!/usr/bin/env python3

from scapy.all import ARP, sniff, Ether, Dot11
import datetime
import csv
from threading import Thread
from readFromConfiguration import *
from scans.actionesAfterDetectoin.sendMacToServer import send_mac_to_server
from scans.actionesAfterDetectoin.deauthentication import deauth


ATTACK_CODE = "MITM"

username = ""   # shulde be initielaized while attack called
arp_req=0.0
arp_res=0.0
mac_addresses = []


def getMonitorr():
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        line = line.replace("\n","")
        splited_line = line.split(":*:")
        if splited_line[0] == "monitor":
            print("monitor: ",splited_line[1])
            return splited_line[1]
        splited_line = []

def getManagedd():
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        line = line.replace("\n","")
        splited_line = line.split(":*:")
        if splited_line[0] == "managed":
            print("managed: ",splited_line[1])
            return splited_line[1]
        splited_line = []


def PacketHandler(packet):
    global arp_req, arp_res, mac_addresses
    if packet.haslayer(ARP) and packet.haslayer(Dot11):
        if packet[ARP].op == ARP.who_has:
            arp_req += 1
        if packet[ARP].op == 2: #ARP.is-at subtype
            print("got arp ans for: ",packet.psrc)
            arp_res += 1
            mac_addresses.append(packet[Dot11].addr2)
        if arp_req != 0 and arp_res / arp_req > 1.5:
            # mitm is occured!
            attacker_mac = max(set(mac_addresses), key=mac_addresses.count)
            action_while_dns_attack(attacker_mac)
            print("mitm occured")
        print("ARP REQUESTS: " ,int(arp_req), " ARP RESPONSES: ", int(arp_res))
        if arp_req != 0 and arp_res !=0 and arp_res / arp_req < 0.1:
            arp_req=0.0
            arp_res=0.0
            mac_addresses=[]    # clear the list.


def action_while_dns_attack(mac):
    global username
    time = datetime.datetime.now()

    Thread(target=deauth, args=([mac])).start()

    #notify server
    send_mac_to_server(username,mac)

    #write to file
    event = [ATTACK_CODE, time.strftime("%d/%m/%Y"), time.strftime("%H:%M:%S"), mac.replace(":","-").upper()]
    with open(r'./temp_log.csv', 'a') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(event)
    csvFile.close()


def mitmScan():
    sniff(iface=getMonitor(), prn=PacketHandler) #noam's interfas: wlx503eaa7d0fd7


if __name__ == "__main__":
    mitmScan()
