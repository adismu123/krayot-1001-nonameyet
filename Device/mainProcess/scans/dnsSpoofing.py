#!/usr/bin/env python3

from scapy.all import sniff, DNS, UDP, DNSRR, Dot11 ,ARP,srp1,Ether
import datetime
import csv
from threading import Thread
import socket
import netifaces
from termcolor import colored
import requests
from readFromConfiguration import *
from scans.actionesAfterDetectoin.sendMacToServer import send_mac_to_server
from scans.actionesAfterDetectoin.deauthentication import deauth

ATTACK_CODE = 'DNS-SPOOFING'

"""
this func will check if a givven ip is a valid ipv4
input:ip
output: true if the ip is in v4 standart else otherwise
"""
def is_valid_ipv4_address(address):
    try:
        socket.inet_pton(socket.AF_INET, address)
    except AttributeError:  # no inet_pton here, sorry
        try:
            socket.inet_aton(address)
        except socket.error:
            return False
        return address.count('.') == 3
    except socket.error:  # not a valid address
        return False
    return True


"""
this func will check if a given ip is a valid ipv6
input:ip
output: true if the ip is in v6 standart else otherwise
"""
def is_valid_ipv6_address(address):
    try:
        socket.inet_pton(socket.AF_INET6, address)
    except socket.error:  # not a valid address
        return False
    return True


"""
this func will ask our api dns query and return if the domain we got in the network was good
input: ip and domain we got from sniffing dns in the network
output: true if the ip we got is correct else otherwise
"""
def askAPI(ip,domain):
    ans = None
    try:
        if is_valid_ipv4_address(ip):
            ans = requests.get('https://api.exana.io/dns/'+domain+'/a', verify=False).json() #ask the domain for answer in v4 standart
            ans = ans["answer"]
            for ipObj in ans:
                if ipObj["rdata"] == ip:
                    return True
        else:
            if is_valid_ipv6_address:
                ans = requests.get('https://api.exana.io/dns/'+domain+'/aaaa', verify=False).json() #ask the domain for answer in v6 standart
                ans = ans["answer"]
                for ipObj in ans:
                    if ipObj["rdata"] == ip:
                        return True
            return False
    except Exception as e:
        print(colored("DNS return False cuz error:", 'red'))
        print(e)
        return False

username = getUsername()

def scan_dns_packets(packet):
    if packet.haslayer(DNS) and packet[UDP].sport==53: #check if dns packet
        if packet.haslayer(DNSRR): #check if dns answer
            #print(packet.show())
            returned_ip = [] #list to store ips
            requested_domain = [] #list to store domains
            for i in range(0, packet[DNS].ancount): #itarete the packet
                if type(packet[DNS][DNSRR][i].rdata) == type(""):
                    returned_ip.append(packet[DNS][DNSRR][i].rdata) #append ip
                    requested_domain.append(packet[DNS][DNSRR][i].rrname.decode("utf-8")) #append domain

            results=[] #list to store the results
            for (i, d) in zip(returned_ip, requested_domain): #itarete the answer from the dns packet
                temp = askAPI(i,d) #get the answer from the api
                if(temp == False):
                    print(colored('under attack', 'red'))
                else:
                    print(colored('Success', 'green'))
                results.append(temp) #append list of results

            flag = True #setting flag

            for res in results: #itarete the results
                if res == False: #see if there is unmatch
                    flag = False #triggering rhe flag
                    break #stops the loop since there is attack

            attacker_mac = packet[Dot11].addr2.replace(":","-").upper() #get the sender of the dns
            router_mac = getMac().replace(":","-").upper() #get the router mac
            print("attacker_mac",attacker_mac)
            print("roter_mac",router_mac)
            if not flag:
                # attack detected
                if attacker_mac == router_mac:
                    action_while_dns_attack_out_of_the_network()
                else:
                    action_while_dns_attack(attacker_mac)

"""
this func will do all the require actions in dns spoofing inside the network
input:attacker mac
output:none
"""
def action_while_dns_attack(mac):
    global username
    time = datetime.datetime.now()
    #notify server
    send_mac_to_server(username,mac)

    #write to file
    event = [ATTACK_CODE, time.strftime("%d/%m/%Y"), time.strftime("%H:%M:%S"), macreplace(":","-").upper()]
    with open(r'./temp_log.csv', 'a') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(event)
    csvFile.close()

    #start deauthentication
    Thread(target=deauth, args=([mac])).start()

"""
this func will do all the require actions in dns spoofing outside the network
input:none
output:none
"""
def action_while_dns_attack_out_of_the_network():
    global username
    time = datetime.datetime.now()

    #write to file
    event = [ATTACK_CODE, time.strftime("%d/%m/%Y"), time.strftime("%H:%M:%S"), "Unknown Attacker."]
    with open(r'./temp_log.csv', 'a') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(event)
    csvFile.close()



if __name__ == "__main__":
    sniff(iface=getMonitor(), prn=scan_dns_packets,filter="type Data")
