#!/usr/bin/env python3

from scapy.all import TCP, sniff, Dot11, IP
from statistics import mode
import time
import datetime
import csv
from threading import Thread
from readFromConfiguration import *
from scans.actionesAfterDetectoin.sendMacToServer import send_mac_to_server
from scans.actionesAfterDetectoin.deauthentication import deauth

username=None

def set_username():
    global username
    username = getUsername()

TCP_PACKETS_COUNTER = 0
ATTACK_CODE = "Syn-Flood"
TCP_PACKETS = {}

MAX_TCP_SYNS_FOR_DST = 25
LOGICAL_SYN_TIME_DIFFRENCE = 4
MAX_TCP_SYNS = 80
SENDER = 0
TIME = 1


def action_while_attack_detected(mac):
    global username
    time = datetime.datetime.now()
    Thread(target=deauth, args=([mac])).start()

    #notify server
    send_mac_to_server(username,mac)

    #write to file
    event = [ATTACK_CODE, time.strftime("%d/%m/%Y"), time.strftime("%H:%M:%S"), mac.replace(":","-").upper()]
    with open(r'./temp_log.csv', 'a') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(event)
    csvFile.close()


def action_while_attack_detected_not_from_the_network():
    global username
    time = datetime.datetime.now()

    #write to file
    event = [ATTACK_CODE, time.strftime("%d/%m/%Y"), time.strftime("%H:%M:%S"), "Multiple Senders."]
    with open(r'./temp_log.csv', 'a') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(event)
    csvFile.close()

def hendle_ACK(packet):
    global TCP_PACKETS_COUNTER
    if packet[IP].dst in TCP_PACKETS:
        for syn_req_seq in TCP_PACKETS[ packet[IP].dst ].keys():
                    if syn_req_seq + 1 == packet[TCP].seq:
                        TCP_PACKETS_COUNTER -= 1
                        del (TCP_PACKETS[ packet[IP].dst ])[ syn_req_seq ]
                        if len(TCP_PACKETS[ packet[IP].dst ]) == 0:
                            del TCP_PACKETS[ packet[IP].dst ]
                        return


def add_to_list(packet, time_arrival):
    global TCP_PACKETS_COUNTER, TCP_PACKETS
    TCP_PACKETS_COUNTER += 1
    # create a tupple with the detailes to the list
    t = (packet[Dot11].addr2 , time.time())
    if packet[IP].dst in TCP_PACKETS:
        TCP_PACKETS[ packet[IP].dst ][ packet[TCP].seq ] = t
    else:
        d = { packet[TCP].seq : t }
        TCP_PACKETS[ packet[IP].dst ] = d


def detect_syn_flood_attack(packet):
    global TCP_PACKETS_COUNTER, TCP_PACKETS, MAX_TCP_SYNS
    # global first_paacket_in_list_time,icmp_list, icmp_dict
    currnt_packet_time = time.time()

    if packet.haslayer(TCP) and packet.haslayer(IP):
        #check if ACK packet:
        if packet[TCP].flags == "A":
            hendle_ACK(packet)
        #check if packet is a SYN packet:
        if packet[TCP].flags == "S":
            #check the TCP-SYN packets amount:
            if TCP_PACKETS_COUNTER < MAX_TCP_SYNS:
                #just add the packet
                add_to_list(packet, currnt_packet_time)
            else:
                #check if attack.
                #check if destination IP is in dictionary:
                if packet[IP].dst in TCP_PACKETS:
                    if len( TCP_PACKETS[ packet[IP].dst ] ) >= MAX_TCP_SYNS_FOR_DST:
                        # there is high suspicion of SYN flooding attack. 2 things to do:

                        # 1. get avarage time between packets:
                        times_sum = 0.0
                        minimal_time = list(TCP_PACKETS[ packet[IP].dst ].values())[0][TIME]
                        for syn_req in TCP_PACKETS[ packet[IP].dst ].values():
                            times_sum += syn_req[TIME]
                            if minimal_time >= syn_req[TIME]:
                                minimal_time = syn_req[TIME]
                        avg_time = (times_sum / len(TCP_PACKETS[ packet[IP].dst ])) - minimal_time

                        # 2. get the most common sender:
                        senders = []
                        attacker = ""
                        for syn_req in TCP_PACKETS[ packet[IP].dst ].values():
                            senders.append( syn_req[SENDER] )
                        try:
                            attacker = mode(senders)
                        except:
                            # senders amount is equal.
                            # we will check if someone is under attack at the next SYN pachet
                            pass
                        if attacker != "" and avg_time < LOGICAL_SYN_TIME_DIFFRENCE:
                            # there is an attck in the network.
                            print("ATTACK!!!")
                            if attacker == getMac():
                                action_while_attack_detected_not_from_the_network()
                            else:
                                action_while_attack_detected(attacker)
                        else:
                            add_to_list(packet,currnt_packet_time)
                    else:
                        add_to_list(packet,currnt_packet_time)
                else:
                    add_to_list(packet,currnt_packet_time)

                if TCP_PACKETS_COUNTER > MAX_TCP_SYNS + 5:
                    TCP_PACKETS = {}
                    TCP_PACKETS_COUNTER = 0



if __name__ == "__main__":
    sniff(prn=detect_syn_flood_attack ,iface="mon0")
