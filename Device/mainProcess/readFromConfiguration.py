#!/usr/bin/env python3

import netifaces
from scapy.all import srp1, Ether, ARP

def getMac():
    """
    this func return the router's MAC
    input:none
    output:router's MAC
    """
    splited_line = []
    f = open(r"./configuration.txt","r") #open the conf file
    lines = f.readlines() #read the file
    f.close() #close the file
    for line in lines: #itarete the lines
        splited_line = line.split(":*:") #spliting line by seperator
        if splited_line[0] == "mac": #check if the line is the mac line
            return splited_line[1][:-1] #return thr router mac
        splited_line = []

def getESSID():
    """
    this func return the network's essid
    input: none
    output: network's essid
    """
    splited_line = []
    f = open(r"./configuration.txt","r") #open the conf file
    lines = f.readlines() #read the file
    f.close() #close the file
    for line in lines: #itarete the lines
        splited_line = line.split(":*:") #spliting line by seperator
        if splited_line[0] == "essid": #check if the essid in the current line
            return splited_line[1][:-1] #return the essid
        splited_line = []

def getMonitor():
    """
    this func return the monitor interface
    input: none
    output: monitor Iface
    """
    splited_line = []
    f = open(r"./configuration.txt","r") #open the conf file
    lines = f.readlines() #read the file
    f.close() #close the file
    for line in lines: #itarete the lines
        line = line.replace("\n","")
        splited_line = line.split(":*:") #spliting line by seperator
        if splited_line[0] == "monitor": #check if the monitor iface is on the current line
            return splited_line[1] #return the monitor iface
        splited_line = []

def getManaged():
    """
    this func return the managed interface
    input: none
    output: managed Iface
    """
    splited_line = []
    f = open(r"./configuration.txt","r") #open the conf file
    lines = f.readlines() #read the file
    f.close() #close the file
    for line in lines: #itarete the lines
        line = line.replace("\n","")
        splited_line = line.split(":*:") #spliting line by seperator
        if splited_line[0] == "managed": #check if the managed iface is on the current line
            return splited_line[1] #return the managed iface
        splited_line = []

def getServerIp():
    """
    this func return the server ip
    input: none
    output: server ip
    """
    splited_line = []
    f = open(r"./configuration.txt","r") #open the conf file
    lines = f.readlines() #read the file
    f.close() #close the file
    for line in lines: #itarete the lines
        line = line.replace("\n","")
        splited_line = line.split(":*:") #spliting line by seperator
        if splited_line[0] == "serverIP": #check if lines contain the server ip
            return splited_line[1] #return the server ip
        splited_line = []

def getUsername():
    """
    this func return the username
    input: none
    output: username
    """
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        if "username" in line:
            line = line.replace("\n","")
            splited_line = line.split(":*:")
            return splited_line[1]

def getDHCP():
    """
    this func will return the DHCP server ip
    input: none
    output: dhcp server ip
    """
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        splited_line = line.split(":*:")
        if splited_line[0] == "dhcp":
            return splited_line[1][:-1]
        splited_line = []


def getDNS():
    """
    this func will return the DNS server ip
    input: none
    output: DNS server ip
    """
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        splited_line = line.split(":*:")
        if splited_line[0] == "dns":
            return splited_line[1][:-1]
        splited_line = []

def getRouterIP():
    """
    this func will return the router ip
    input: none
    output: router ip
    """
    splited_line = []
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines:
        splited_line = line.split(":*:")
        if splited_line[0] == "routerIP":
            return splited_line[1][:-1]
        splited_line = []
