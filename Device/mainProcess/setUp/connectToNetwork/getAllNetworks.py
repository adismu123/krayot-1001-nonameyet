#!/usr/bin/env python3

from scapy.all import sniff, Dot11
import time


scanning_timeout = 120  # variable that sets the time period that the scan will take
available_networks={"noam":"3"}
ap_mac_list = []
ap_names_list = []

def packet_handler (packet):
    if packet.haslayer(Dot11):
        if packet.type == 0 and packet.subtype == 8:
            if packet.addr2 not in ap_mac_list:
                ap_mac_list.append(packet.addr2)
                ap_names_list.append(packet.info)

def get_networks(scanning_duration):
    global available_networks
    sniff(iface="mon0", prn=packet_handler, timeout=scanning_duration+2)
    ap_macs_and_names={}
    print("SSID founded:")
    for name, mac_addr in zip(ap_names_list, ap_mac_list):
        ap_macs_and_names[mac_addr] = name.decode('utf-8')
    
    return ap_macs_and_names
    
    # return ap_macs_and_names
