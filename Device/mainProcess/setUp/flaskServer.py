#!/usr/bin/env python3

import time
import socket
from multiprocessing.pool import ThreadPool
import netifaces
from flask import Flask, redirect, render_template, request
from setUp.connectToNetwork.getAllNetworks import get_networks, available_networks
from setUp.connectToNetwork.connectUsingEssidAndPass import connect_to_net
from setUp.connectToNetwork.getAllNetworksAndCreypto import get_all

a_n = None

pool = ThreadPool(processes=1)

app = Flask(__name__)
scanning_duration = 45

time_started = None

async_result = None

def writeConf(essid,routerMac,security):
    splited_line = []
    gws=netifaces.gateways()
    router_ip,iface = gws['default'][netifaces.AF_INET]
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for i in range(len(lines)):
        splited_line = lines[i].replace("\n","").split(":*:")
        if splited_line[0] == "essid":
            lines[i] = splited_line[0]+":*:"+essid+"\n"
        if splited_line[0] == "mac":
            lines[i] = splited_line[0]+":*:"+routerMac.upper().replace(":","-")+"\n"
        if splited_line[0] == "routerIP":
            lines[i] = splited_line[0]+":*:"+router_ip+"\n"
        if splited_line[0] == "dns":
            lines[i] = splited_line[0]+":*:"+router_ip+"\n"
        if splited_line[0] == "dhcp":
            lines[i] = splited_line[0]+":*:"+router_ip+"\n"
        if splited_line[0] == "security":
            lines[i] = splited_line[0]+":*:"+security+"\n"
        splited_line = []
    f = open(r"./configuration.txt","w")
    f.writelines(lines)
    f.close()


@app.route('/showNetworks')
def show_networks():
    time.sleep(1)
    a_n = async_result.get().values()
    print (a_n)
    apn=[]
    for tupple in a_n:
        apn.append(tupple[0])
    return render_template("availiblesNets.html", networks=apn)

def shutdown_server():
    time.sleep(2)
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()



@app.route('/sendData', methods=["GET", "POST"])
def receive_data():
    if request.method == "POST":
        ssid = request.form["ssid"]
        password = request.form["password"]
        res = connect_to_net(ssid,password)
        if "successfully activated" not in res:
            return "<h2>Error: wrong details</h2>"
        a_n = async_result.get()
        print ("********************")
        for mac, _ssid_ in a_n.items():
            if ssid == _ssid_[0]:
                IPAddr = socket.gethostbyname(socket.gethostname())
                shutdown_server()
                writeConf(ssid,mac,_ssid_[1])
                return render_template("afterConnected.html", ssid=ssid, password=password, mac=mac, security=_ssid_[1], IPAddr=IPAddr)
                return "ssid: " + ssid + "<br>password: " + password + "<br><br>mac: " + mac + "<br>security: " + _ssid_[1] + "<br><h2>Device IP: " + IPAddr + ":3000</h2>"
    return redirect("/")


@app.route('/')
@app.route('/index')
def index():
   if scanning_duration-int(time.time() - time_started) > 3:
        return render_template("index.html", duration=3+scanning_duration-int(time.time() - time_started))
   else:
       return render_template("index.html", duration=3)


def main():
    global time_started, async_result
    time_started = time.time()
    async_result = pool.apply_async(get_all, [scanning_duration] )
    reloadNetworks()
    app.run(host="0.0.0.0",debug=False)


@app.route('/reloadNetworks')
def reloadNetworks():
    global time_started, async_result
    time_started = time.time()
    async_result = pool.apply_async(get_all, [scanning_duration] )
    return redirect('/index')


if __name__ == "__main__":
    main()
    print("run")
#    async_result = pool.apply_async(get_networks,args=(scanning_duration),callback=a)
    print ("start serving")
