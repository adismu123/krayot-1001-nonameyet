from threading import Thread, Lock
import socket
from collections import deque
import os
import time

def padNum(num):
    if (num>=1 and num<=9):
        return "0"+str(num)
    return str(num)
    

#this func is between the server and the device
def serverFunc(q,lock,clientQueue,clientQueueLock,ip,port):
    #go is a variable that say until when we need to run the device (change it to True to stop running)
    go = False
    while(not go):#every time server-client socket closes its open new one (till go = True)
        #key = setKey()
        # Create a TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        msg = None
        # Connect the socket to the port where the server is listening
        server_address = (ip, port)
        sock.connect(server_address)
        try:
            while(True):
                #lock in order to get in the q
                lock.acquire()
                try:
                    #check if q is empty
                    if(len(q)>0):
                        msg = q.popleft()
                finally:
                    #release mutex
                    lock.release()
                if(msg != None):
                    #case there is a massege
                    #sending msg to server
                    sock.sendall(msg)
                    #reciving server response
                    ans = sock.recv(4096)
                    #lock client-device queue
                    clientQueueLock.acquire()
                    try:
                        #appending client-device queue
                        clientQueue.append(ans)
                    finally:
                        #releace client queue
                        clientQueueLock.release()
                #set msg to null
                msg = None
        except Exception as e:
            print(e)
        finally:
            #closing server-device socket
            sock.close()
    os._exit(1)


#########################################################################################################
#########################################################################################################


#this func is between the client and the device
def clientFunc(client_address, connection, q,lock,serverQueue,serverQueueLock):
    go = False
    usernameLen = 0
    username = ""
    try:
        #print('client thread: connection from', client_address)
        
        # Receive the data
        data = connection.recv(4096)
        #print("data: ",end="")
        #print(data)
        usernameLen = int(data.decode("utf-8")[19:21])
        username = data.decode("utf-8")[21:21+usernameLen]
        #print ("client thread: data recived: ", data.decode("utf-8"))
        
        #lock server q
        serverQueueLock.acquire()
        try:
            #insert into server q
            serverQueue.append(data)
            #print ("client thread: serverQueue appended")
        finally:
            #releace server q
            serverQueueLock.release()
        #print ("client thread: check if q got response from server")
        #this loop will wait to the responce from the server will get inside client q
        while(not go):
            #try to read the response from server
            #lock the client q
            lock.acquire()
            try:
                #check if client q is empty
                if(len(q)>0):
                    #take out the msg from the client q
                    msg = q.popleft()

                    if (msg.decode("utf-8")[4:4+int((msg.decode("utf-8")[2:4]))] == username):
                        msg = msg.decode("utf-8")
                        #print("msg: " , msg)
                        msg = msg.replace(padNum(usernameLen)+username,"",1)
                        msg = msg.encode("ascii")
                        #print("Client: " + username + "/tmsg: " + msg.decode("utf-8"))
                        #print ("client thread: got response from server: ", msg.decode("utf-8"))
                        go = True #in order to get out of the while
                    else:
                        q.appendleft(msg)
             
            finally:
                #releace the client q
                lock.release()
        #send the client the response from the server
        connection.sendall(msg)
    except Exception as e:
        print(e)
    finally:
        #print ("client thread: closing socket")
        #closing socket
        connection.close()


def startConnectingHttpToServer(server_ip,server_port):
    qServer = deque()
    qServerLock = Lock()
    qClient = deque()
    qClientLock = Lock()
    serverThread = Thread(target=serverFunc, args=(qServer,qServerLock,qClient,qClientLock,server_ip,server_port))
    serverThread.start()

    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the port
    server_address = ('localhost', 15963)
    #print('starting up on {} port {}'.format(*server_address))
    sock.bind(server_address)

  

    while(True):
        try:
            # Listen for incoming connections
            sock.listen(1)
            conn, addr = sock.accept()
            clientThread = Thread(target=clientFunc, args=(addr,conn,qClient,qClientLock,qServer,qServerLock))
            clientThread.start()
        except Exception as e:
            print("ERROR: ",e)
