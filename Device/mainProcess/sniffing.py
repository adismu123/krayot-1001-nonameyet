#!/usr/bin/env python3

from scapy.all import sniff,ARP,srp1,Ether
import netifaces

from readFromConfiguration import *
from scans.realTimeMitmScan import PacketHandler as man_in_the_middle_scan
from scans.dnsSpoofing import scan_dns_packets as dns_spoofing_scan
from scans.dhcpScan import handle_dhcp_packet
from scans.evilTwinScan import evil_twin_scan, essid, bssid
from scans.synFloodingScan import detect_syn_flood_attack
from scans.scanForNewConnections import scan_arp_requests


devices=[]
from scans.smurfAttack import detect_smarf_attack



##############################################
#############   configuration    #############
interface=getMonitor()
num_of_packets_to_sniff_in_session = 15
encripted_network_flag = False
##############################################
#############       flags        #############
man_in_the_middle_flag = False
dhcp_spoofing_flag = True
dns_spoofing_flag = False
smurf_flag = False
syn_flood_flag = False
evil_twin_flag = False
##############################################

def set_flags():
    """
    this function will set the scan's flags
    input: none
    output: none
    """
    global man_in_the_middle_flag, dhcp_spoofing_flag, dns_spoofing_flag, smurf_flag, evil_twin_flag,syn_flood_flag
    splited_line = []
    #read the configuration file
    f = open(r"./configuration.txt","r")
    lines = f.readlines()
    f.close()
    for line in lines: #itarete the lines of the conf file
        line = line.replace('\n',"")
        splited_line = line.split(":*:")
        if splited_line[0] == "mitm_flag": #check if MITM flag
            man_in_the_middle_flag = (splited_line[1] == '1') #set flag
        elif splited_line[0] == "dhcp-spoofing_flag": #check if DHCP flag
            dhcp_spoofing_flag = (splited_line[1] == '1') #set flag
        elif splited_line[0] == "dns-spoofing_flag": #check if DNS flag
            dns_spoofing_flag = (splited_line[1] == '1') #set flag
        elif splited_line[0] == "Smurf_flag": #check if Smurf flag
            smurf_flag = (splited_line[1] == '1') #set flag
        elif splited_line[0] == "Syn-Flood_flag": #check if SYN flag
            syn_flood_flag = (splited_line[1] == '1') #set flag
        elif splited_line[0] == "Evil-Twin_flag": #check if Evil twin flag
            evil_twin_flag = (splited_line[1] == '1') #set flag
        splited_line = []


def sort_to_scans(packet):
    """
    this func will sort the incoming packet to the check scans functions
    input: incoming packet
    output: none
    """
    # decript packet (if needed)
    if encripted_network_flag:
        pass #doesn't work over secured network at the moment

    # send packets to scans functiones according to client request
    if man_in_the_middle_flag:
        man_in_the_middle_scan(packet)

    if dhcp_spoofing_flag:
        handle_dhcp_packet(packet)

    if dns_spoofing_flag:
        try:
            dns_spoofing_scan(packet)
        except Exception as e:
            print("DNS Exception:",e)

    if evil_twin_flag:
        evil_twin_scan(packet)

    if len(devices) > 0:
        scan_arp_requests(packet)

    if smurf_flag:
        detect_smarf_attack(packet)

    if syn_flood_flag:
        print("in syn Flood")
        detect_syn_flood_attack(packet)


def startSniffing(_essid,_bssid):
    """
    this func will start to sniff packets
    input: network essid, network bssid
    output: none
    """
    global essid, bssid
    print(1)
    essid = _essid
    bssid = _bssid
    set_flags() #set the scan's flags
    while True:
        sniff(count=num_of_packets_to_sniff_in_session,store=0,prn=sort_to_scans,iface=interface) #start sniffing
        set_flags() #setting the flags in case they changed


if __name__ == "__main__":
    startSniffing(getESSID(), getMac())
