import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
import datetime


def send_mail(send_from, send_to, subject, text, files=None):
    """
    this func will send mail
    input: destination email, sourse email, subject, text and files
    output: none
    """
    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(text))

    for f in files or []:
        with open(f, "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=basename(f)
            )
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        msg.attach(part)
    print("starting process")
    smtp = smtplib.SMTP("smtp.gmail.com", 587)
    smtp.ehlo()
    smtp.starttls()
    smtp.login(send_from, "Net54321")
    print("log in done")
    smtp.sendmail(send_from, send_to, msg.as_string())
    print("mail sent")
    smtp.close()
    print("success!")

if __name__ == '__main__':
    now = datetime.datetime.now()
    dst_addr = input("Enter the client Email address: ")
    _subject = "Network Scanner - Log file"
    _msg_text = "Thank you for using Network Scanner!\nThis file sent to you on "\
                + now.strftime("%d/%m/%Y") + " at " + now.strftime("%H:%M:%S")
    _file_path = [r"C:\Users\Noam\Desktop\2002.jpg"]
    send_mail(send_from="networkscanner1@gmail.com", send_to=dst_addr, subject=_subject, text=_msg_text, files=_file_path)
