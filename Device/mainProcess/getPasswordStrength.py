#!/usr/bin/env python3

import re

def password_check(password):
    """
    this func will return the grade of a password
    input: password to check
    output: grade for the password

    Verify the strength of 'password'
    Returns a dict indicating the wrong criteria
    _______________________________________________________
    A password is considered strong if: |   gread for trait
    ____________________________________|__________________
        8 characters length or more     |   25 %
        1 digit or more                 |   25 %
        1 symbol or more                |   10 %
        1 uppercase letter or more      |   15 %
        1 lowercase letter or more      |   25 %
    ____________________________________|__________________
        total                           |   100 %
    _______________________________________________________
    """

    # calculating the length
    length_error = len(password) < 8
    if length_error:
        length_greade = 0
    else:
        length_greade = 25

    # searching for digits
    digit_error = re.search(r"\d", password) is None
    if digit_error:
        digit_greade = 0
    else:
        digit_greade = 25

    # searching for uppercase
    uppercase_error = re.search(r"[A-Z]", password) is None
    if uppercase_error:
        uppercase_greade = 0
    else:
        uppercase_greade = 15

    # searching for lowercase
    lowercase_error = re.search(r"[a-z]", password) is None
    if lowercase_error:
        lowercase_greade = 0
    else:
        lowercase_greade = 25

    # searching for symbols
    symbol_error = re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~"+r'"]', password) is None
    if symbol_error:
        symbol_greade = 0
    else:
        symbol_greade = 10

    # overall result
    password_ok = not ( length_error or digit_error or uppercase_error or lowercase_error or symbol_error )

    return {
        'password_ok' : length_greade + digit_greade + uppercase_greade + lowercase_greade + symbol_greade,
        'length' : length_greade,
        'digit' : digit_greade,
        'uppercase' : uppercase_greade,
        'lowercase' : lowercase_greade,
        'symbol' : symbol_greade
    }


if __name__ == "__main__":
    password = input("Enter password to check it strength: ")
    print(password_check(password))
