﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;

namespace Server
{
    class Helper
    {
        //MsgTypeClient is an enum of types of msg types
        public enum MsgTypeClient
        {
            LoginType = 1,
            RegisterType = 3,
            LogFileGivenType = 5,
            LogFilesReqType = 7,
            LogFileReqType = 9,
            AuthorizationChangeType = 11,
            CheckMacAddressType = 13,
            DisconnectType = 15,
            AddToBlackList = 17,
            CheckAuth = 19,
            verifyDNSrequest = 21,
            emailReq = 23
        }

        public enum RegisterPacket
        {
            password = 0,
            mail = 1,
            birthDate = 2,
            country = 3
        }

        public enum AddScanPacket
        {
            scanName = 0,
            date = 1,
            routerMac = 2
        }


        public enum dnsPacket
        {
            ip = 0,
            domain = 1
        }

        /*this func wiil returnt the msgType of an incomung msg*/
        public static MsgTypeClient getMessageTypeCode(Socket sock)
        {
            string type = getStringPartFromSocket(sock, 2);
            MsgTypeClient ans;
            Enum.TryParse<MsgTypeClient>(type, out ans);
            return ans;
        }

        /*
        this func will return string wich its len is bytesNum of an incomin msg 
        */
        public static string getStringPartFromSocket(Socket sc, int bytesNum)
        {
            byte[] buffer = new byte[bytesNum];
            int recived;
            try
            {
                recived = sc.Receive(buffer, 0, bytesNum, SocketFlags.None);
            }
            catch
            {
                throw new Exception("Error in socket -  closing ");
            }
            return Encoding.UTF8.GetString(buffer);
        }

        /*
        this func will return int wich its string len is bytesNum of an incomin msg 
        */
        public static int getIntPartFromSocket(Socket sc, int bytesNum)
        {
            return int.Parse(getStringPartFromSocket(sc, bytesNum));
        }

        /*
        this func will return the sha256 of a string value 
        */
        public static string sha256_hash(string value)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(value));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        /*
         *this func will pad a number into five digit format
         */
        public static string padNum5Times(int num)
        {
            if (num >= 0 && num <= 9)
            {
                return "0000" + num.ToString();
            }
            if (num >= 10 && num <= 99)
            {
                return "000" + num.ToString();
            }
            if (num >= 100 && num <= 999)
            {
                return "00" + num.ToString();
            }
            if (num >= 1000 && num <= 9999)
            {
                return "0" + num.ToString();
            }
            return num.ToString();
        }

        /*
         *this func will pad a number into two digit format
         */
        public static string padNum(int num)
        {
            if (num >= 0 && num <= 9)
            {
                return "0" + num.ToString();
            }
            return num.ToString();
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
    }
}
