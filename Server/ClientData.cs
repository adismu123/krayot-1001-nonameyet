﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;

namespace Server
{
    class ClientData
    {
        private Socket _clientSocket;
        private string _username;
        private string _macAddress;

        public ClientData(Socket clientSocket, string username, string mac)
        {
            Console.WriteLine("client connected");
            //start default username
            this._username = username;
            //set default mac
            this._macAddress = mac;

            this._clientSocket = clientSocket;
        }

        //username property
        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        //macaddress property
        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }
    }
}
