﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    class SocketClient
    {
        private Socket _socket;
        private Thread _socketThread;
        private RSAParameters _socketPrivateKey; //socket private key

        public SocketClient(Socket socket, Queue<Packet> queue, Mutex mtxPackets, Dictionary<string, ClientData> users, Mutex mtxUsers)
        {
            this._socket = socket;
            //start a thread that sniff for request and push them inside of the packet queue
            this._socketThread = new Thread(() => SocketHandler(queue, mtxPackets, users, mtxUsers));
            this._socketThread.Start();
        }

        //socket property
        public Socket SocketProperty
        {
            get { return _socket; }
            set { _socket = value; }
        }


        /*
         *this func will take care of the client 
         * it will sniff for requests and build a new packet 
         * for each request. if there is a isuse it will delete 
         * the client out of the clients list
         */
        private void SocketHandler(Queue<Packet> packets, Mutex mtxPackets, Dictionary<string, ClientData> users, Mutex mtxUsers)
        {
            Helper.MsgTypeClient msgType;
            bool endConnection = false;
            string macAddress;
            string username = "";
            bool dc = false;
            int usernameLen;
            string msg = "";
            int len = 0;
            try
            {
                Console.WriteLine("SocketHandler for client started");
                Console.WriteLine("socket handler: waiting for requests from client");
                while (!endConnection)
                {
                    msg = "";
                    msgType = Helper.getMessageTypeCode(this._socket); //getting the msgType
                    Console.WriteLine("socket handler: got massege code: " + msgType);
                    macAddress = Helper.getStringPartFromSocket(this._socket, 17); //getting the mackAddress
                    Console.WriteLine("socket handler: got mac address: " + macAddress);
                    usernameLen = Helper.getIntPartFromSocket(this._socket, 2); //getting the username len
                    Console.WriteLine("socket handler: got UN Len: " + usernameLen.ToString());
                    username = Helper.getStringPartFromSocket(this._socket, usernameLen); //getting the username
                    Console.WriteLine("socket handler: got username: " + username);



                    if (msgType == Helper.MsgTypeClient.RegisterType || msgType == Helper.MsgTypeClient.LoginType || msgType == Helper.MsgTypeClient.DisconnectType)
                    {
                        dc = false;
                    }
                    else
                    {
                        mtxUsers.WaitOne();
                        if (!users.ContainsKey(username))
                        {
                            dc = true;
                        }
                        mtxUsers.ReleaseMutex();
                    }



                    //check if the user wanted to disconnect
                    if (msgType == Helper.MsgTypeClient.DisconnectType)
                    {
                        mtxUsers.WaitOne();
                        if (users.ContainsKey(username))
                        {
                            users.Remove(username);
                        }
                        mtxUsers.ReleaseMutex();
                        //if (this._socketUsers.Count == 0)
                        //{
                        //    endConnection = true;
                        //}
                        byte[] byteAns = Encoding.UTF8.GetBytes("16" + Helper.padNum(username.Length) + username + "C");
                        this._socket.Send(byteAns, 0, byteAns.Length, SocketFlags.None);
                    }
                    else
                    {
                        switch (msgType)
                        {
                            case Helper.MsgTypeClient.LoginType:
                                msg = Helper.getStringPartFromSocket(this._socket, 64);
                                break;
                            case Helper.MsgTypeClient.RegisterType:
                                msg = Helper.getStringPartFromSocket(this._socket, 64) + ":*:"; //add password + seperetion 
                                len = Helper.getIntPartFromSocket(this._socket, 2);//get mail len
                                msg += Helper.getStringPartFromSocket(this._socket, len) + ":*:"; //add mail + seperation
                                msg += Helper.getStringPartFromSocket(this._socket, 10) + ":*:";// add birth date + seperation
                                len = Helper.getIntPartFromSocket(this._socket, 2);//get country len
                                msg += Helper.getStringPartFromSocket(this._socket, len);//add country 
                                break;
                            case Helper.MsgTypeClient.LogFilesReqType:
                                msg = "";
                                break;
                            case Helper.MsgTypeClient.AddToBlackList:
                                msg = Helper.getStringPartFromSocket(this._socket, 17);
                                break;
                            case Helper.MsgTypeClient.CheckMacAddressType:
                                msg = Helper.getStringPartFromSocket(this._socket, 17);
                                break;
                            case Helper.MsgTypeClient.AuthorizationChangeType:
                                msg = Helper.getStringPartFromSocket(this._socket, 1) + ":*:";
                                len = Helper.getIntPartFromSocket(this._socket, 2);//get username2 len
                                msg += Helper.getStringPartFromSocket(this._socket, len);
                                break;
                            case Helper.MsgTypeClient.LogFileReqType:
                                msg = Helper.getStringPartFromSocket(this._socket, 32);
                                break;
                            case Helper.MsgTypeClient.CheckAuth:
                                msg = "";
                                break;
                            case Helper.MsgTypeClient.LogFileGivenType:
                                len = Helper.getIntPartFromSocket(this._socket, 2);//get the len of the scan name
                                msg += Helper.getStringPartFromSocket(this._socket, len) + ":*:";//get the scan name
                                msg += Helper.getStringPartFromSocket(this._socket, 10) + ":*:";// add date
                                msg += Helper.getStringPartFromSocket(this._socket, 17) + ":*:";// add routerMac
                                {
                                    int holes = Helper.getIntPartFromSocket(this._socket, 2); //get the number of attackes
                                    for (int i = 0; i < holes; i++)
                                    {
                                        len = Helper.getIntPartFromSocket(this._socket, 2);//get the len of the hole name
                                        msg += Helper.getStringPartFromSocket(this._socket, len) + ":*:";//get the hole name
                                        msg += Helper.getStringPartFromSocket(this._socket, 17) + ":*:";//get the attacker
                                        msg += Helper.getStringPartFromSocket(this._socket, 10) + ":*:";//get the date of the attack
                                        msg += Helper.getStringPartFromSocket(this._socket, 8) + ":*:";//get the hour of the attack
                                    }
                                }
                                msg = msg.Substring(0, msg.Length - 3);
                                break;
                            case Helper.MsgTypeClient.verifyDNSrequest:
                                len = Helper.getIntPartFromSocket(this._socket, 2); //get the len of the ip address
                                msg += Helper.getStringPartFromSocket(this._socket, len) + ":*:";    //get IP
                                len = Helper.getIntPartFromSocket(this._socket, 2); //get the len of the domain
                                msg += Helper.getStringPartFromSocket(this._socket, len); // get domain
                                break;
                            case Helper.MsgTypeClient.emailReq:
                                msg = "";
                                break;
                            default:
                                Console.WriteLine("Error: SocketHandler Got To Default... MSG_TYPE: " + msgType.ToString());
                                break;
                        }
                        Console.WriteLine("socket handler: checkStats return true");
                        if (dc)
                        {
                            mtxUsers.WaitOne();
                            if (users.ContainsKey(username))
                            {
                                users.Remove(username);
                            }
                            mtxUsers.ReleaseMutex();
                            //if (this._socketUsers.Count == 0)
                            //{
                            //    endConnection = true;
                            //}
                            byte[] byteAns = Encoding.UTF8.GetBytes("99" + Helper.padNum(username.Length) + username + "D");
                            this._socket.Send(byteAns, 0, byteAns.Length, SocketFlags.None);
                        }
                        else
                        {
                            mtxPackets.WaitOne();
                            packets.Enqueue(new Packet(macAddress, username, msgType, msg, this._socket));
                            mtxPackets.ReleaseMutex();
                            Console.WriteLine("created new packet");
                        }
                        dc = false;
                    }
                }
            }
            catch (Exception e)
            {
                //print the exeption that got
                Console.WriteLine("client handler: " + e.Message);
                mtxUsers.WaitOne();
                if (users.ContainsKey(username))
                {
                    users.Remove(username);
                }
                mtxUsers.ReleaseMutex();
            }
            finally
            {
                Console.WriteLine("socket close");
                this._socket.Close();
            }
        }
        

    }
}

