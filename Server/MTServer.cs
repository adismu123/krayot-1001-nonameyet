﻿//libs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data;
using System.Collections;
using System.Data.OleDb;
using System.Text.RegularExpressions;

namespace Server
{
    class MTServer
    {
        private Socket _listeningSocket;
        private Mutex _packetsMutex;
        private Queue<Packet> _packets;
        private string _connectionString; //this is the string that connects between 
        private Dictionary<string, ClientData> _users;
        private Mutex _usersMutex;

        public MTServer()
        {
            //creat socket to listen
            this._listeningSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //check error in socket
            if (this._listeningSocket == null)
            {
                Exception e = new Exception("ERROR OPEN SOCKET");
                throw e;
            }

            //connection string in order to run connection with the data base
            this._connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=F:\Magshimim\DB4WEBSITE\DB.accdb;Persist Security Info=False";
            this._packets = new Queue<Packet>();
            this._packetsMutex = new Mutex();
            this._users = new Dictionary<string, ClientData>();
            this._usersMutex = new Mutex();

            //binding socket
            IPEndPoint ipe = new IPEndPoint(IPAddress.Any, 5656);
            this._listeningSocket.Bind(ipe);
            Console.WriteLine("Starts listening... IP = " + ipe.Address + " PORT = " + ipe.Port);

            //thread to listen non stop for clients 
            Thread listenThred = new Thread(ListenThread);
            listenThred.Start();

            //thread that take care of the packets q
            Thread queueHandler = new Thread(RequestHandler);
            queueHandler.Start();
        }

        /*
         * this func will accepting clients non stop
         */
        private void ListenThread()
        {
            while (true)
            {
                //starts listen
                this._listeningSocket.Listen(0);

                //add client to clients
                new SocketClient(this._listeningSocket.Accept(), this._packets, this._packetsMutex, this._users, this._usersMutex);
            }
        }

        /*
         * this func will take care of the packets q
         */
        private void RequestHandler()
        {
            Packet p = null;
            string macAddress, username = "", msg;
            Socket userSocket = null;
            Helper.MsgTypeClient msgType;
            try
            {
                while (true)//run non stop
                {
                    //lock the packets mutex q in order to check if there is a packet
                    this._packetsMutex.WaitOne();
                    //check if q is empty
                    if (this._packets.Count > 0)
                    {
                        p = this._packets.Dequeue();
                    }
                    //releace packets mutex
                    this._packetsMutex.ReleaseMutex();
                    //check if there is a packet 
                    if (p != null)
                    {
                        //takes the details out of the packet
                        macAddress = p.MacAddress;
                        username = p.Username;
                        msgType = p.MsgType;
                        msg = p.Msg;
                        userSocket = p.UserSocket;
                        switch (msgType)
                        {
                            case Helper.MsgTypeClient.LoginType:
                                handleLoginRequest(macAddress, username, msg, userSocket);
                                break;
                            case Helper.MsgTypeClient.RegisterType:
                                handleRegisterRequest(macAddress, username, msg, userSocket);
                                break;
                            case Helper.MsgTypeClient.LogFilesReqType:
                                handleFilesReq(macAddress, username, userSocket);
                                break;
                            case Helper.MsgTypeClient.AddToBlackList:
                                handleAddToBlackList(username, msg, userSocket);
                                break;
                            case Helper.MsgTypeClient.CheckMacAddressType:
                                handleChackMac(username, msg, userSocket);
                                break;
                            case Helper.MsgTypeClient.AuthorizationChangeType:
                                handleAuthorizationChange(macAddress, username, msg, userSocket);
                                break;
                            case Helper.MsgTypeClient.LogFileReqType:
                                handleGetFileReq(macAddress, username, msg, userSocket);
                                break;
                            case Helper.MsgTypeClient.CheckAuth:
                                handleCheckAuth(macAddress, username, userSocket);
                                break;
                            case Helper.MsgTypeClient.LogFileGivenType:
                                handleAddScan(macAddress, username, msg, userSocket);
                                break;
                            case Helper.MsgTypeClient.emailReq:
                                handleEmailReq(macAddress, username, userSocket);
                                break;
                            case Helper.MsgTypeClient.verifyDNSrequest:
                                handleDNSReq(macAddress, username, userSocket, msg);
                                break;
                            default:
                                Console.WriteLine("RequestHandler: got to default");
                                break;
                        }
                    }
                    //set p to null
                    p = null;
                }
            }
            catch (Exception ex)
            {
                this._usersMutex.WaitOne();
                if (this._users.ContainsKey(username))
                {
                    //disconnect the user
                    this._users.Remove(username);
                }
                this._usersMutex.ReleaseMutex();
                //notify the user we are disconnecting him
                byte[] byteAns = Encoding.UTF8.GetBytes("99" + Helper.padNum(username.Length) + username + "D");
                userSocket.Send(byteAns, 0, byteAns.Length, SocketFlags.None);
            }
        }

        /*
         * this func will take care in login case
         */
        private void handleLoginRequest(string macAddress, string username, string password, Socket userSocket)
        {
            string ans = "02" + Helper.padNum(username.Length) + username;
            //check if the user exist un the DB 
            this._usersMutex.WaitOne();
            if (this._users.ContainsKey(username))
            {
                ans += "D1";
            }
            else if (IsExistInDB(username, password))
            {
                //confirm (exist in db and not connected)
                ans += "C0";
                this._users.Add(username, new ClientData(userSocket, username, macAddress));
            }
            else
            {
                //aint exist in db
                ans += "D2";
            }
            this._usersMutex.ReleaseMutex();
            //send the response
            byte[] byteAns = Encoding.UTF8.GetBytes(ans);
            userSocket.Send(byteAns, 0, byteAns.Length, SocketFlags.None);
        }

        /*
         *this func will handle register requests 
         */
        private void handleRegisterRequest(string macAddress, string username, string msg, Socket userSocket)
        {
            bool wrongDetails = false;
            string[] arr = msg.Split(new string[] { ":*:" }, StringSplitOptions.None); //splititng the msg by the seperetor 
            if (IsUsernameExist(username))
            {
                //user exist
                byte[] byteAns = Encoding.UTF8.GetBytes("04" + Helper.padNum(username.Length) + username + "D2");
                userSocket.Send(byteAns, 0, byteAns.Length, SocketFlags.None);
            }
            else if ((username.All(char.IsLetterOrDigit) == false) || (username.Length < 3))
            {
                //check username
                wrongDetails = true;
            }
            else if (!Regex.IsMatch(arr[(int)Helper.RegisterPacket.mail], @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$", RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)))
            {
                //check email
                wrongDetails = true;
            }
            else if (arr[(int)Helper.RegisterPacket.mail].Length < 10)
            {
                //check email len
                wrongDetails = true;
            }
            else
            {
                //addidng user into users table
                byte[] byteAns = Encoding.UTF8.GetBytes("04" + Helper.padNum(username.Length) + username + "C0");
                ArrayList prmList = new ArrayList(); //new prm list for the query

                prmList.Add(new OleDbParameter("@username", username)); //add the username
                prmList.Add(new OleDbParameter("@password", arr[(int)Helper.RegisterPacket.password])); //add the password
                prmList.Add(new OleDbParameter("@birthDate", DateTime.Parse(arr[(int)Helper.RegisterPacket.birthDate]))); //add the birthDate
                prmList.Add(new OleDbParameter("@country", arr[(int)Helper.RegisterPacket.country])); //add the country
                prmList.Add(new OleDbParameter("@email", arr[(int)Helper.RegisterPacket.mail])); //add the email

                bool ans = DataBaseHelper.ExecuteSPNonQuery(this._connectionString, "spInsertUser", prmList); //execute query
                if (GetNumAdminsOfProduct(GetIdProduct(macAddress)) <= 0) //case no admins to the product
                {
                    ChangeAuth(GetIdUser(username), GetIdProduct(macAddress), 2);//add the first user as admin
                }
                userSocket.Send(byteAns, 0, byteAns.Length, SocketFlags.None); //send response
            }
            if (wrongDetails)//case of wrong details
            {
                byte[] byteAns = Encoding.UTF8.GetBytes("04" + Helper.padNum(username.Length) + username + "D1");
                userSocket.Send(byteAns, 0, byteAns.Length, SocketFlags.None);
            }
        }

        /*
         *this func will handle the get scans req
         */
        private void handleFilesReq(string macAddress, string username, Socket userSocket)
        {
            int idProduct = GetIdProduct(macAddress); //get id of product
            int idUser = GetIdUser(username); //get id of user
            int authorizationLvl = GetAuthorizationLvl(idProduct, idUser); //get user authorization level
            if (authorizationLvl == -1)//case user dont have authorization
            {
                byte[] byteAns = Encoding.UTF8.GetBytes("08" + Helper.padNum(username.Length) + username + "D");
                userSocket.Send(byteAns, 0, byteAns.Length, SocketFlags.None);
            }
            else
            {                                                                         //function that takes the id of the product and return a string of his scans 
                string res = "08" + Helper.padNum(username.Length) + username + "C" + ParseScansToString(idProduct);//setting res
                Console.WriteLine("res on get files: " + res);
                byte[] byteAns = Encoding.UTF8.GetBytes(res);
                userSocket.Send(byteAns, 0, byteAns.Length, SocketFlags.None);
            }
        }

        /*
         *this func will add mac into the black-list table  
         */
        private void handleAddToBlackList(string username, string mac, Socket userSocket)
        {
            string res = "18" + Helper.padNum(username.Length) + username;
            try
            {
                if (!IsAttackerExist(mac))
                {
                    ArrayList prmList = new ArrayList(); //new prm list for the query

                    prmList.Add(new OleDbParameter("@MacAddress", mac)); //add the mac

                    bool ans = DataBaseHelper.ExecuteSPNonQuery(this._connectionString, "spInsertBlackListMacAddress", prmList); //execute query
                }
                res += "C";
            }
            catch
            {
                res += "D";
            }
            finally
            {
                byte[] byteAns = Encoding.UTF8.GetBytes(res);
                userSocket.Send(byteAns, 0, byteAns.Length, SocketFlags.None);
            }
        }

        /*
         this func will check if th MAC exist in the DB  
         */
        private void handleChackMac(string username, string mac, Socket userSocket)
        {
            string res = "14" + Helper.padNum(username.Length) + username;

            ArrayList prmList = new ArrayList(); //new prm list for the query

            prmList.Add(new OleDbParameter("@MacAddress", mac)); //add the mac
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectBlackListByMAC", prmList); // send to the DB wraper the query and the prm list
            if (dt.Rows.Count >= 1)
            {
                res += "B"; //case exist
            }
            else
            {
                res += "F"; //case not exist
            }

            byte[] byteAns = Encoding.UTF8.GetBytes(res);
            userSocket.Send(byteAns, 0, byteAns.Length, SocketFlags.None);
        }


        private void handleAuthorizationChange(string macAddress, string username, string msg, Socket userSocket)
        {

            ///need to delete autho when its exist and need to dbug it

            int idProduct = GetIdProduct(macAddress); //get id of product
            int idUser = GetIdUser(username); //get id of user
            int authorizationLvl = GetAuthorizationLvl(idProduct, idUser); //get user authorization level
            if (authorizationLvl != 2)//case user dont have admin to the product
            {
                byte[] byteAns = Encoding.UTF8.GetBytes("12" + Helper.padNum(username.Length) + username + "D1");
                userSocket.Send(byteAns, 0, byteAns.Length, SocketFlags.None);
                return;
            }
            string[] arr = msg.Split(new string[] { ":*:" }, StringSplitOptions.None); //splititng the msg by the seperetor 
            int level = int.Parse(arr[0]);
            string username2 = arr[1];
            int idUser2 = GetIdUser(username2); //get id of user2
            if (idUser2 == -1)
            {
                byte[] byteAns = Encoding.UTF8.GetBytes("12" + Helper.padNum(username.Length) + username + "D2");
                userSocket.Send(byteAns, 0, byteAns.Length, SocketFlags.None);
                return;
            }
            if (ChangeAuth(idUser2, idProduct, level) == 0)
            {
                //all good
                byte[] ans = Encoding.UTF8.GetBytes("12" + Helper.padNum(username.Length) + username + "C0");
                userSocket.Send(ans, 0, ans.Length, SocketFlags.None);
            }
            else
            {
                //user is the only admin and it cannot be deleted
                byte[] ans = Encoding.UTF8.GetBytes("12" + Helper.padNum(username.Length) + username + "D3");
                userSocket.Send(ans, 0, ans.Length, SocketFlags.None);
            }
        }

        //the func return the user a parse string with the scan detailes.
        private void handleGetFileReq(string macAddress, string username, string msg, Socket userSocket)
        {
            string ans = "10" + Helper.padNum(username.Length) + username + ParseScanResultsToString(msg, GetIdUser(username));
            byte[] res = Encoding.UTF8.GetBytes(ans);
            userSocket.Send(res, 0, ans.Length, SocketFlags.None);
        }

        //the func returns the user it's authorization level.
        private void handleCheckAuth(string macAddress, string username, Socket userSocket)
        {
            // check the authorization
            int auth = GetAuthorizationLvl(GetIdProduct(macAddress), GetIdUser(username));
            // 
            if (auth == -1)
            {
                auth = 0;
            }
            string msg = "20" + Helper.padNum(username.Length) + username + auth.ToString();
            byte[] ans = Encoding.UTF8.GetBytes(msg);
            userSocket.Send(ans, 0, ans.Length, SocketFlags.None);
        }


        private void handleAddScan(string macAddress, string username, string msg, Socket userSocket)
        {
            string res, attackName = "";
            bool attackStatus = false, flag = false;
            DateTime attackTime=new DateTime();
            string attacker;
            int attackerID=0;
            string key = Guid.NewGuid().ToString().Replace("-", "");
            int scanId = 0;
            Dictionary<string, int> goodAttacks = fillAttackesDictionary();
            string[] arr = msg.Split(new string[] { ":*:" }, StringSplitOptions.None); //splititng the msg by the seperetor 
            int numOfHols = arr.Length - 3; //3 is the amount of must have cells

            int auth = GetAuthorizationLvl(GetIdProduct(macAddress), GetIdUser(username));
            
            // check if the user is permited to request the log and create scan
            // or if the scan has results. (results = events) 
            if (auth == -1 || numOfHols == 0)
            {
                //return the client it's request denied
                res = "06" + Helper.padNum(username.Length) + username + "D";
            }
            else
            {
                // add scan detailes to the DB for scans history record

                // add scan 'dry' details:
                ArrayList prmList = new ArrayList();
                prmList.Add(new OleDbParameter("@displayName", arr[(int)Helper.AddScanPacket.scanName]));
                prmList.Add(new OleDbParameter("@idUser", GetIdUser(username)));
                prmList.Add(new OleDbParameter("@idProduct", GetIdProduct(macAddress)));
                 prmList.Add(new OleDbParameter("@scanDate", DateTime.Parse(arr[(int)Helper.AddScanPacket.date])));
                prmList.Add(new OleDbParameter("@routerMac", arr[(int)Helper.AddScanPacket.routerMac]));
                prmList.Add(new OleDbParameter("@key", key));
                DataBaseHelper.ExecuteSPNonQuery(this._connectionString, "spInsertScan", prmList); //execute query
                // get the scan id to link the events to the scan
                scanId = GetScanIdByKey(key);
                //go over all events in the log:
                for (int i = 3; i < arr.Length; i++)
                {
                    // in order to avoid sttacks on the network.
                    // in normak usage, it sould always get into that 'if' condition.
                    if (goodAttacks.Keys.Contains(arr[i].ToUpper()))
                    {
                        attackName = arr[i];
                        attacker = arr[i + 1];
                        attackerID = getAttackerID(attacker);
                        attackTime = DateTime.Parse(arr[i + 2] + " " + arr[i + 3]);
                        flag = true;
                    }
                    if (flag)
                    {
                        //insert event into the DB:
                        ArrayList prmListAddResult = new ArrayList();
                        prmListAddResult.Add(new OleDbParameter("@idScan", scanId));
                        prmListAddResult.Add(new OleDbParameter("@idSecurityHole", goodAttacks[attackName.ToUpper()]));
                        prmListAddResult.Add(new OleDbParameter("@attackTime", attackTime));
                        prmListAddResult.Add(new OleDbParameter("@attackerID", attackerID));
                        DataBaseHelper.ExecuteSPNonQuery(this._connectionString, "spInsertScanResult", prmListAddResult); //execute query
                    }
                    //reset the flag
                    flag = false;
                }
                // build a seccess message
                res = "06" + Helper.padNum(username.Length) + username + "C" + key;
            }
            // send the result to the user:
            byte[] ans = Encoding.UTF8.GetBytes(res);
            userSocket.Send(ans, 0, ans.Length, SocketFlags.None);
        }


        private void handleEmailReq(string macAddress, string username, Socket userSocket)
        {
            try
            {
                int idProduct = GetIdProduct(macAddress);
                int idUser = GetIdUser(username);
                List<string> emails = GetAdminsEmails(idProduct);
                string userEmail = GetUserEmail(idUser);
                if (emails.Contains(userEmail) == false)
                {
                    emails.Add(userEmail);
                }
                // build a string contains a list of users's emails:
                string res = "24" + Helper.padNum(username.Length) + username + "C" + Helper.padNum(emails.Count);
                foreach (string s in emails)
                {
                    res += Helper.padNum(s.Length) + s;
                }
                // send the message
                byte[] ans = Encoding.UTF8.GetBytes(res);
                userSocket.Send(ans, 0, ans.Length, SocketFlags.None);
            }
            catch (Exception e)
            {
                string res = "24" + Helper.padNum(username.Length) + username + "D";
                byte[] ans = Encoding.UTF8.GetBytes(res);
                userSocket.Send(ans, 0, ans.Length, SocketFlags.None);
            }
        }

        // won't be in use since we are using an API.
        private void handleDNSReq(string macAddress, string username, Socket userSocket, string msg)
        {
            bool match = false;
            string res = "22" + Helper.padNum(username.Length) + username;
            string[] arr = msg.Split(new string[] { ":*:" }, StringSplitOptions.None); //splititng the msg by the seperetor 
            IPAddress[] ipAddr = Dns.GetHostAddresses(arr[(int)Helper.dnsPacket.domain]);
            Console.WriteLine("DNS got ip:\t" + arr[(int)Helper.dnsPacket.ip] + "\tgot domain:\t" + arr[(int)Helper.dnsPacket.domain]);
            foreach (IPAddress ip in ipAddr)
            {
                Console.WriteLine("ip:\t" + ip.ToString());
                if (ip.ToString() == arr[(int)Helper.dnsPacket.ip])
                {
                    res += "T";
                    Console.WriteLine("DNS answer: true");
                    match = true;
                }
            }
            if (!match)
            {
                res += "F";
                Console.WriteLine("DNS answer: false");
            }
            byte[] ans = Encoding.UTF8.GetBytes(res);
            userSocket.Send(ans, 0, ans.Length, SocketFlags.None);
        }


        /****************************************************************************
         * this func will check if a givven username and password exist in the DB   *
         * it will return true if so and false if they dont                         *
         ****************************************************************************/
        private bool IsExistInDB(string username, string password)
        {
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@username", username)); //add the username
            prmList.Add(new OleDbParameter("@password", password)); //add the password
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectUserByLoginStats", prmList); // send to the DB wraper the query and the prm list
            return dt.Rows.Count == 1; // return answer
        }

        /****************************************************************************
         * this func will check if the username allready exist in the DB            *
         * it will return true if it exist and false othewise                       *
         ****************************************************************************/
        private bool IsUsernameExist(string username)
        {
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@username", username)); //add the username
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectUserByUsername", prmList); // send to the DB wraper the query and the prm list
            return dt.Rows.Count != 0; // return answer
        }

        /*
         * this func will get the MAC of a product and return his id
         */
        private int GetIdProduct(string macAddress)
        {
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@productMacAddress", macAddress));
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectProductIdByMac", prmList); // send to the DB wraper the query and the prm list
            if (dt.Rows.Count == 1) //check if exist
            {
                //return product id
                return int.Parse(dt.Rows[0]["idProduct"].ToString());
            }
            //throw error in order to dc user
            throw new Exception("Error in get product id (product not exist)");
        }

        /*
         * this func will get the username of a user and return his id
         */
        private int GetIdUser(string username)
        {
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@username", username)); //add the username
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectUserIdByUsername", prmList); // send to the DB wraper the query and the prm list
            if (dt.Rows.Count == 1)
            {
                //return user id
                return int.Parse(dt.Rows[0]["idUser"].ToString());
            }
            else
            {
                return -1;
            }
            //throw error in order to dc user
            throw new Exception("Error in get user id (user not exist)");
        }

        /*
         * this func will get the id of a product and the id of a user and 
         * return his authorization level (-1 if he doesnt have authoraized)
         */
        private int GetAuthorizationLvl(int idProduct, int idUser)
        {
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@idUser", idUser));
            prmList.Add(new OleDbParameter("@idProduct", idProduct));
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectAuthorization", prmList); // send to the DB wraper the query and the prm list
            if (dt.Rows.Count == 1)
            {
                //return user authorization level to the product
                return int.Parse(dt.Rows[0]["authorizationLevel"].ToString());
            }
            else
            {
                // user does not exist ot it has no authorization for the product.
                return -1;
            }
        }

        private bool IsAttackerExist(string mac)
        {
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@MacAddress", mac));
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectNumOfBlackListExist", prmList); // send to the DB wraper the query and the prm list
            return int.Parse(dt.Rows[0][0].ToString()) != 0;
        }


        private int getAttackerID(string mac)
        {

            if (!IsAttackerExist(mac))
            {
                ArrayList addToDB = new ArrayList(); //new prm list for the query

                addToDB.Add(new OleDbParameter("@MacAddress", mac)); //add the mac

                DataBaseHelper.ExecuteSPNonQuery(this._connectionString, "spInsertBlackListMacAddress", addToDB); //execute query
            }
            ArrayList prmList = new ArrayList(); //new prm list for the query

            prmList.Add(new OleDbParameter("@MacAddress", mac)); //add the mac

            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectIdAttackerByMAc", prmList); // send to the DB wraper the query and the prm list
            return int.Parse(dt.Rows[0][0].ToString());
        }


        /*
         * this func will get the id of a product and return the number of admins that product have 
         */
        private int GetNumAdminsOfProduct(int idProduct)
        {
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@idProduct", idProduct));
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectNumAdminsOfProduct", prmList); // send to the DB wraper the query and the prm list
            if (dt.Rows.Count == 1)
            {
                //return user authorization level to the product
                return int.Parse(dt.Rows[0][0].ToString());
            }
            else
            {
                // no record found. so num of admins is 0.
                return 0;
            }
        }

        /*
         * this func will change the authentication of a user to a product
         * the function get the id of the user to change auth the id of the product and the level [0=no auth,1=user,2=admin]
         * if the request is delete auth then the func will check if the user is an admin and if he is 
         * it will check the number of admins if he is the only admin than it wont let him change the auth
         */
        private int ChangeAuth(int idUser, int idProduct, int level)
        {
            bool delUser = false;
            // check if the user have auth to the product if so it wiil change the flag in order to delete the auth
            if (UserAuthProduct(idUser, idProduct)) 
            {
                delUser = true;
            }
            if (level != 0)
            {
                if (delUser)
                {
                    deleteUserAuthFromProduct(idUser, idProduct); //delete current auth between the user and the product
                }
                ArrayList prmList = new ArrayList(); //new prm list for the query
                prmList.Add(new OleDbParameter("@idUser", idUser)); //add the id of user2
                prmList.Add(new OleDbParameter("@idProduct", idProduct)); //add the id of user2
                prmList.Add(new OleDbParameter("@authorizationLevel", level)); //add the id of user2

                DataBaseHelper.ExecuteSPNonQuery(this._connectionString, "spInsertAuthorizationTbl", prmList); //execute query
            }
            else
            {
                //check if the user is admin and if he is the only admin
                if (GetAuthorizationLvl(idProduct, idUser) == 2 && GetNumAdminsOfProduct(idProduct) == 1) 
                {
                    return 1;
                }
                ArrayList prmList = new ArrayList(); //new prm list for the query

                prmList.Add(new OleDbParameter("@idUser", idUser)); //add the id of user2
                prmList.Add(new OleDbParameter("@idProduct", idProduct)); //add the id of user2

                DataBaseHelper.ExecuteSPNonQuery(this._connectionString, "spDeleteAuthorization", prmList); //execute query
            }
            return 0;
        }

        /*
         * this func will get and id of a user and an id of a product and will return if the user
         * has auth to the product
         */
        private bool UserAuthProduct(int idUser, int idProduct)
        {
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@idProduct", idProduct)); //add the id of the product
            prmList.Add(new OleDbParameter("@idUser", idUser));
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectHasAuth", prmList); // send to the DB wraper the query and the prm list
            return int.Parse(dt.Rows[0][0].ToString()) > 0;
        }

        /*
         *this func will get an id of a user and an id of a product and will delete the auth 
         * that exist between them
         */
        private void deleteUserAuthFromProduct(int idUser, int idProduct)
        {
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@idUser", idUser));
            prmList.Add(new OleDbParameter("@idProduct", idProduct)); //add the id of the product
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spDeleteAuthorization", prmList); // send to the DB wraper the query and the prm list
        }

        /*
         * this func will get an id of a scan and will return the id of the product that took it
         */
        private int GetIdProductByIdScan(int idScan)
        {
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@idScan", idScan));
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectIdProductByScan", prmList); // send to the DB wraper the query and the prm list
            return int.Parse(dt.Rows[0][0].ToString());
        }

        //almost same as the func above.
        // this func returns the product id by given key (which is another uniqe id for every scan.
        private int GetIdProductByScanKey(string key)
        {
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@key", key));
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectIdProductByScanKey", prmList); // send to the DB wraper the query and the prm list
            return int.Parse(dt.Rows[0][0].ToString());
        }

        //
        private int GetScanIdByKey(string key)
        {
            //spSelectScanIdByKey
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@key", key));
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectScanIdByKey", prmList); // send to the DB wraper the query and the prm list
            if (dt.Rows.Count == 1)
            {
                return int.Parse(dt.Rows[0][0].ToString());
            }
            else
            {
                return -1;
            }
        }

        //the func fills a dictionaty with the attacks and thirs IDs in order to avoid sql injection, scripts or some other attacks can cause damage to our server.
        private Dictionary<string,int> fillAttackesDictionary()
        {
            Dictionary<string, int> d = new Dictionary<string, int>();
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectSecurityHoles");
            foreach(DataRow dr in dt.Rows)
            {
                d.Add(dr["securityHoleName"].ToString().ToUpper(), int.Parse(dr["idSecurityHole"].ToString()));
            }
            return d;
        }

        //the func returns the admins email in order to notify them when someone ask for the log.
        private List<string> GetAdminsEmails(int idProduct)
        {
            List<string> emails = new List<string>();
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@idProduct", idProduct));
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectProductAdminsEmails", prmList); // send to the DB wraper the query and the prm list
            foreach (DataRow dr in dt.Rows)
            {
                Console.WriteLine("Email: " + dr[0].ToString());
                emails.Add(dr[0].ToString());
            }
            return emails;
        }
        /*
         * the func return the users email by it's ID
         */
        private string GetUserEmail(int idUser)
        {
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@idUser", idUser));
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectUserEmailById", prmList); // send to the DB wraper the query and the prm list
            return dt.Rows[0][0].ToString();
        }


        /*
        *this func will get the id of a product and 
        * then will ask for his scans,after that the 
        * function will creat a string that represent the scans
        */
        private string ParseScansToString(int idProduct)
        {
            string ans = "";
            ArrayList prmList = new ArrayList(); //new prm list for the query
            prmList.Add(new OleDbParameter("@idProduct", idProduct)); //add the id of the product
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectProductScans", prmList); // send to the DB wraper the query and the prm list
            //go over all scans
            foreach (DataRow dr in dt.Rows)
            {
                //go over every parm of the scan. each iteration go into another case.
                foreach (DataColumn dc in dt.Columns)
                {
                    switch (dc.ColumnName)
                    {
                        case "key":
                            ans += dr[dc].ToString(); //add the key
                            break;
                        case "displayName":
                            ans += Helper.padNum(dr[dc].ToString().Length) + dr[dc].ToString(); //add the len of the name and the name of the scan
                            break;
                        case "scanDate":
                            ans += Helper.padNum(DateTime.Parse(dr[dc].ToString().Split(' ')[0]).Day) + "/" + Helper.padNum(DateTime.Parse(dr[dc].ToString().Split(' ')[0]).Month) + "/" + (DateTime.Parse(dr[dc].ToString().Split(' ')[0]).Year).ToString(); //add the date 
                            break;
                        case "productMacAddress":
                            ans += dr[dc].ToString(); //add the product mac
                            break;
                        case "routerMac":
                            ans += dr[dc].ToString(); //add the router mac
                            break;
                        case "username":
                            ans += Helper.padNum(dr[dc].ToString().Length) + dr[dc].ToString(); //add the username of the user that took the scan
                            break;
                    }
                }
            }
            return ans;
        }


        /*
         * this func will get an id of a scan and an id of a user and will create a string 
         * that present it
         * if the user dont have auth to the product it wont give him the results of the scan
         */
        private string ParseScanResultsToString(string key, int idUser)
        {
            string ans = "";
            DateTime attackTime;
            int idProduct = GetIdProductByScanKey(key);
            if (!UserAuthProduct(idUser, idProduct))
            {
                ans = "D";
                return ans;
            }
            ans += "C";
            ArrayList prmList = new ArrayList(); //new prm list for the query
            int idScan = GetScanIdByKey(key);
            prmList.Add(new OleDbParameter("@idScan", idScan));
            DataTable dt = DataBaseHelper.GetDataTableByDataReader(this._connectionString, "spSelectScanResult", prmList); // send to the DB wraper the query and the prm list

            ans += Helper.padNum(dt.Rows[0]["username"].ToString().Length);//added responsible user len 
            ans += dt.Rows[0]["username"].ToString(); //add user
            ans += dt.Rows[0]["productMacAddress"].ToString(); //add mac address
            ans += Helper.padNum(dt.Rows[0]["displayName"].ToString().Length); //add scan name len
            ans += dt.Rows[0]["displayName"].ToString(); //add scan name 
            ans += Helper.padNum(DateTime.Parse(dt.Rows[0]["scanDate"].ToString().Split(' ')[0]).Day) + "/" + Helper.padNum(DateTime.Parse(dt.Rows[0]["scanDate"].ToString().Split(' ')[0]).Month) + "/" + Helper.padNum(DateTime.Parse(dt.Rows[0]["scanDate"].ToString().Split(' ')[0]).Year); //add scan date

            ans += dt.Rows[0]["routerMac"].ToString(); //added router mac


            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    switch (dc.ColumnName)
                    {
                        case "securityHoleName":
                            ans += Helper.padNum(dr[dc].ToString().Length); //add the len of the name of the security hole
                            ans += dr[dc].ToString(); //add the security hole name
                            break;
                        case "attackTime":
                            attackTime = DateTime.Parse(dr[dc].ToString());
                            ans += Helper.padNum(attackTime.Day) + "/" + Helper.padNum(attackTime.Month) + "/" + attackTime.Year.ToString() + Helper.padNum(attackTime.Hour) + ":" + Helper.padNum(attackTime.Minute) + ":" + Helper.padNum(attackTime.Second);
                            break;
                        case "MacAddress":
                            ans += dr[dc].ToString();
                            break;
                    }
                }
            }
            return ans;
        }

    }
}
