﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Packet
    {
        private string _macAddress;
        private string _username;
        private Helper.MsgTypeClient _msgType;
        private string _msg;
        private Socket _s;
        
        public Packet(string macAddress, string username, Helper.MsgTypeClient msgType, string msg, Socket s)
        {
            this._macAddress = macAddress;
            this._msg = msg;
            this._msgType = msgType;
            this._username = username;
            this._s = s;
        }

        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        public Socket UserSocket
        {
            get { return _s; }
            set { _s = value; }
        }

        public Helper.MsgTypeClient MsgType
        {
            get { return _msgType; }
            set { _msgType = value; }
        }

        public string Msg
        {
            get { return _msg; }
            set { _msg = value; }
        }
    }
}
